﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

// keii: Mediaindex.exeからの移植

namespace Tvmaid
{
    internal interface IYomi
    {
        string GetYomi(string src);
    }

    internal class IFELang : IDisposable
    {
        public IFELang()
        {
            ifelang = Activator.CreateInstance(Type.GetTypeFromProgID("MSIME.Japan")) as IFELanguage;
            if (ifelang == null)
                throw new Exception("かな変換の初期化に失敗しました。");

            int hr = ifelang.Open();
            if (hr != 0)
                throw Marshal.GetExceptionForHR(hr);
        }

        public void Dispose()
        {
            ifelang.Close();
        }

        public string Conv(string src)
        {
            string text = null;
            if (src.Length < 100)
            {
                int phonetic = ifelang.GetPhonetic(src, 1, -1, out text);
                if (phonetic != 0)
                    throw Marshal.GetExceptionForHR(phonetic);
            }

            if (text != null)
                return text;

            string text2 = "";
            int i = 0;
            string str;
            int phonetic2;
            bool flag = false;

            while (i < src.Length)
            {
                text2 += src[i].ToString();
                char c = src[i];
                if (c <= ')')
                {
                    if (c == ' ' || c == '(' || c == ')')
                        flag = true;
                }
                else
                {
                    switch (c)
                    {
                        case '-':
                        case '[':
                        case '\\':
                        case ']':
                            flag = true;
                            break;
                        default:
                            if (c == '\u3000')
                                flag = true;

                            break;
                    }
                }

                if (flag)
                {
                    phonetic2 = ifelang.GetPhonetic(text2, 1, -1, out str);
                    if (phonetic2 != 0)
                        throw Marshal.GetExceptionForHR(phonetic2);

                    text += str ?? Strings.StrConv(text2, VbStrConv.Wide, 0);
                    text2 = "";
                }

                i++;
            }

            phonetic2 = ifelang.GetPhonetic(text2, 1, -1, out str);
            if (phonetic2 != 0)
                throw Marshal.GetExceptionForHR(phonetic2);

            text += str;            
            return text;
        }

        private IFELanguage ifelang;

        // IFELanguage2 Interface ID
        //[Guid("21164102-C24A-11d1-851A-00C04FCC6B14")]
        [ComImport]
        [Guid("019F7152-E6DB-11d0-83C3-00C04FDDB82E")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IFELanguage
        {
            int Open();
            int Close();
            int GetJMorphResult(uint dwRequest, uint dwCMode, int cwchInput, [MarshalAs(UnmanagedType.LPWStr)] string pwchInput, IntPtr pfCInfo, out object ppResult);
            int GetConversionModeCaps(ref uint pdwCaps);
            int GetPhonetic([MarshalAs(UnmanagedType.BStr)] string @string, int start, int length, [MarshalAs(UnmanagedType.BStr)] out string result);
            int GetConversion([MarshalAs(UnmanagedType.BStr)] string @string, int start, int length, [MarshalAs(UnmanagedType.BStr)] out string result);
        }
    }

    internal class Roma
    {
        public Roma()
        {
            this.def = new PairList(Util.GetUserPath("roma.def"));
            this.def.Load();
        }

        public string Conv(string src)
        {
            foreach (KeyValuePair<string, string> current in this.def)
            {
                src = src.Replace(current.Key, current.Value);
            }
            StringBuilder stringBuilder = new StringBuilder(src);
            for (int i = 0; i < stringBuilder.Length; i++)
            {
                if (stringBuilder[i] == 'っ' && i + 1 < stringBuilder.Length)
                {
                    if (char.IsLetter(stringBuilder[i + 1]))
                    {
                        stringBuilder[i] = stringBuilder[i + 1];
                    }
                    else
                    {
                        stringBuilder[i] = 't';
                    }
                }
            }
            return stringBuilder.ToString();
        }

        private PairList def;
    }
}

