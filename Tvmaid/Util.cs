﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Linq;

namespace Tvmaid
{
    //拡張メソッド
    static class StringExtension
    {
        public static string Formatex(this string format, params object[] values)
        {
            return string.Format(format, values);
        }

        public static bool CompareNC(this string str1, string str2)
        {
            return string.Compare(str1, str2, true) == 0;
        }

        public static int ToInt(this string s)
        {
            return int.Parse(s);
        }

        public static double ToDouble(this string s)
        {
            return double.Parse(s);
        }

        public static long ToLong(this string s)
        {
            return long.Parse(s);
        }

        public static DateTime ToDateTime(this string s)
        {
            return DateTime.Parse(s);
        }

        // keii
        public static string ToKatakana(this string s)
        {
            return new string(s.Select(c => (c >= 'ぁ' && c <= 'ゖ') ? (char)(c + 'ァ' - 'ぁ') : c).ToArray());
        }

        public static string ToHiragana(this string s)
        {
            return new string(s.Select(c => (c >= 'ァ' && c <= 'ヶ') ? (char)(c + 'ぁ' - 'ァ') : c).ToArray());
        }

        static public string ToZenAlphaNum(this string s)
        {
            var str = new string(s.Select(c => (c >= '0' && c <= '9') ? (char)(c - '0' + '０') : c).ToArray());
            str = new string(str.Select(c => (c >= 'a' && c <= 'z') ? (char)(c - 'a' + 'ａ') : c).ToArray());
            return new string(str.Select(c => (c >= 'A' && c <= 'Z') ? (char)(c - 'A' + 'Ａ') : c).ToArray());
        }

        static public string ToHanAlphaNum(this string s)
        {
            var str = new string(s.Select(c => (c >= '０' && c <= '９') ? (char)(c - '０' + '0') : c).ToArray());
            str = new string(str.Select(c => (c >= 'ａ' && c <= 'ｚ') ? (char)(c - 'ａ' + 'a') : c).ToArray());
            return new string(str.Select(c => (c >= 'Ａ' && c <= 'Ｚ') ? (char)(c - 'Ａ' + 'A') : c).ToArray());
        }
    }

    //関数
    public static class Util
    {
        //実行ファイルのフォルダ
        public static string GetBasePath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        public static string GetBasePath(string file)
        {
            return Path.Combine(GetBasePath(), file);
        }

        //Webサーバルートフォルダ
        public static string GetWwwRootPath()
        {
            return Path.Combine(GetBasePath(), "wwwroot");
        }

        //ユーザ設定フォルダ
        public static string GetUserPath()
        {
            return Path.Combine(GetBasePath(), "user");
        }

        public static string GetUserPath(string file)
        {
            return Path.Combine(GetUserPath(), file);
        }

        public static string GetTempPath()
        {
            return Path.Combine(Path.GetTempPath(), "tvmaid");
        }

        //ユーザ設定ファイルがなければコピー
        public static void CopyUserFile()
        {
            var user = Util.GetUserPath();
            if (Directory.Exists(user) == false)
                Directory.CreateDirectory(user);
            /*
            var dir = new DirectoryInfo(Path.Combine(Util.GetBasePath(), "default"));
            var files = dir.GetFiles();

            foreach (var file in files)
            {
                var path = Path.Combine(user, file.Name);

                if (File.Exists(path) == false)
                    file.CopyTo(path);
            }
            */
            CopyDirectory(Path.Combine(GetBasePath(), "default"), Path.Combine(GetBasePath(), "user"));
        }

        public static void CopyPlugin()
        {
            try
            {
                var src = Util.GetBasePath("TvmaidPlugin.tvtp");
                var tvtest = AppDefine.Main.Data["tvtest"];
                var dir = Path.Combine(Path.GetDirectoryName(tvtest), "Plugins");
                var dest = Path.Combine(dir, "TvmaidPlugin.tvtp");

                if (File.Exists(dest))
                {
                    if (File.GetLastWriteTime(src) == File.GetLastWriteTime(dest))
                    {
                        Log.Info("Tvmaidプラグイン OK");
                        return;
                    }
                }

                File.Copy(src, dest, true);
                Log.Info("Tvmaidプラグインを更新しました。");
            }
            catch (Exception ex)
            {
                throw new Exception("TVTestのプラグインフォルダに、Tvmaidプラグインをコピーできませんでした。[詳細]" + ex.Message);
            }
        }

        // keii
        public static List<string> GetRecPath()
        {
            List<string> recFolder = new List<string> { AppDefine.Main.Data["record.folder"] };

            // for spare folder
            if (AppDefine.Main.Data.IsDefined("record.folder.spare") == true) // スペアフォルダ設定の有無確認
                if (AppDefine.Main.Data["record.folder.spare"].Length != 0)
                    recFolder.Add(AppDefine.Main.Data["record.folder.spare"]);

            // for tuner folder
            using (var tvdb = new Tvdb(true))
            {
                tvdb.Sql = "select name from tuner";
                var tname = tvdb.GetList();
                for (int i = 0; i < tname.Count; i++)
                {
                    if (AppDefine.Main.Data.IsDefined("record.folder.tuners." + tname[i][0]) == true) // チューナ別フォルダ設定の有無確認
                    {
                        var fname = AppDefine.Main.Data["record.folder.tuners." + tname[i][0]];
                        if (fname.Length != 0)
                        {
                            recFolder.Add(AppDefine.Main.Data["record.folder.tuners." + tname[i][0]]);
                        }
                    }
                }
            }
            return recFolder;
        } 

        public static void MakeThumbnail(string file, int id, int smargein, int duration, int mode = 0, string outpath = "")
        {
            var dir = outpath == "" ? Util.GetUserPath("thumb"): outpath;

            if (Directory.Exists(dir) == false)
                Directory.CreateDirectory(dir);

            var ffmpeg = Util.GetBasePath("ffmpeg.exe");
            if (File.Exists(ffmpeg) == false) return;

            for (int i = (mode  < 2 ? 0 : 1); i < (mode != 1 ? 2 : 1); i++) // mode 0: 通常, 1:1x1だけ作成, 2:3x4だけ作成
            {
                string imgpath = Path.Combine(dir, id + (i == 0 ? "" : ".m") + ".jpg");

                var p = new Process();
                p.StartInfo.FileName = ffmpeg;

                if (Path.GetExtension(file) == ".ts") {
                    int f = (duration > 1800 ? 1800 : duration) / 12; // 12 mean "count of PICs for 'tile'"
                    p.StartInfo.Arguments = (i == 0
                        ? //string.Format("-ss " + (smargein) + " -skip_frame nokey -i  \"{0}\" -vf framestep=10,scale=96:54 -frames:v 1 -vsync 0 \"{1}\"", file, imgpath) :
                        string.Format("-y -ss {2} -i  \"{0}\" -vf thumbnail=600,scale=96:54 -frames:v 1 -vsync 0 \"{1}\"", file, imgpath, smargein)
                        : string.Format("-y -ss {2} -skip_frame nokey -i  \"{0}\" -vf framestep={3},scale=96:54,tile=3x4 -frames:v 1 -vsync 0 \"{1}\"", file, imgpath, smargein, f)
                    );
                }
                else
                {
                    int f = (duration < 90 ? 1 : (duration > 1800 ? 1800 : duration) * 30 / 225 / 12); // 30 mean frame/Sec, 225 mean 300 x 0.75, 12 mean "count of PICs for 'tile'"
                    p.StartInfo.Arguments = (i == 0 
                        ? string.Format("-y -i  \"{0}\" -vf thumbnail=600,scale=96:54 -frames:v 1 -vsync 0 \"{1}\"", file, imgpath)
                        : string.Format("-y -skip_frame nokey -i  \"{0}\" -vf thumbnail={2},scale=96:54,tile=3x4 -frames:v 1 -vsync 0 \"{1}\"", file, imgpath, f)
                    );
                }
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = false;
                p.Start();
                p.PriorityClass = ProcessPriorityClass.BelowNormal; // プライオリティをチョイ下げ
                p.WaitForExit();
                p.Close();
            }
        }

        public static void RemoveThumbnail(int id, string path = "")
        {
            var dir = path == "" ? Util.GetUserPath("thumb") : path;

            if (Directory.Exists(dir) == false)
                return;

            for (int i = 0; i < 2; i++)
            {
                var f = Path.Combine(dir, id + "{0}.jpg".Formatex(i == 0 ? "" : ".m"));
                if (File.Exists(f)) {
                    ShellFile.Delete(f);
                }
            }
        }

        public static void CopyDirectory(string SrcPath, string DestPath, bool Overwrite = false)
        {
            if (!Directory.Exists(DestPath))
            {
                Directory.CreateDirectory(DestPath);
                File.SetAttributes(DestPath, File.GetAttributes(SrcPath));
                Overwrite = true;
            }

            if (Overwrite)
            {
                foreach (string CopyFrom in Directory.GetFiles(SrcPath))
                {
                    string CopyTo = Path.Combine(DestPath, Path.GetFileName(CopyFrom));
                    File.Copy(CopyFrom, CopyTo, true);
                }
            }
            else
            {
                foreach (string CopyFrom in Directory.GetFiles(SrcPath))
                {
                    string CopyTo = Path.Combine(DestPath, Path.GetFileName(CopyFrom));

                    if (!File.Exists(CopyTo))
                    {
                        File.Copy(CopyFrom, CopyTo, false);
                    }
                }
            }

            foreach (string CopyFrom in Directory.GetDirectories(SrcPath))
            {
                string stCopyTo = Path.Combine(DestPath, Path.GetFileName(CopyFrom));
                CopyDirectory(CopyFrom, stCopyTo, Overwrite);
            }
        }
        // 
    }

    //mutexを使いやすくしたクラス
    class MutexEx : IDisposable
    {
        Mutex mutex;

        public MutexEx(string name, int timeout)
        {
            try
            {
                mutex = new Mutex(false, name);

                if (mutex.WaitOne(timeout) == false)
                    throw new Exception("時間内に処理できませんでした。");
            }
            //放棄された場合(AbandonedMutexException)も正常終了とする
            catch (AbandonedMutexException) { }
        }

        public void Dispose()
        {
            try
            {
                mutex.ReleaseMutex();
                mutex.Close();
            }
            catch { }
        }
    }

    //キー=値のファイル
    public class PairList : List<KeyValuePair<string, string>>
    {
        string path;

        public PairList(string path)
        {
            this.path = path;
        }

        public int GetInt(string key)
        {
            return this[key].ToInt();
        }

        public float GetFloat(string key)
        {
            return (float)Convert.ToDouble(this[key]);
        }

        public string this[string key]
        {
            set
            {
                for (var i = 0; i < this.Count; i++)
                {
                    if (this[i].Key == key)
                    {
                        this[i] = new KeyValuePair<string, string>(key, value);
                        return;
                    }
                }
                this.Add(new KeyValuePair<string, string>(key, value));
            }
            get
            {
                foreach (var pair in this)
                    if (pair.Key == key) return pair.Value;

                return null;
            }
        }

        public void Save()
        {
            using (var sw = new StreamWriter(path, false, Encoding.GetEncoding("utf-8")))
            {
                foreach (var pair in this)
                    sw.WriteLine(pair.Key + "=" + pair.Value);
            }
        }

        public void Load()
        {
            using (var sr = new StreamReader(path, Encoding.GetEncoding("utf-8")))
            {
                var text = sr.ReadToEnd();
                string[] lines = text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string line in lines)
                {
                    //コメント行
                    if (line.Length >= 2 && line.StartsWith("//")) { continue; }

                    int sepa = line.IndexOf('=');
                    if (sepa == -1) continue; 

                    var key = line.Substring(0, sepa);
                    var val = "";

                    if (sepa + 1 < line.Length)
                        val = line.Substring(sepa + 1);

                    this[key] = val;
                }
            }
        }

        public bool IsDefined(string key)
        {
            foreach (var pair in this)
                if (pair.Key == key) return true;

            return false;
        }
    }
}
