﻿using System;
using System.Collections.Generic; // keii
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Tvmaid
{
    //ライブと録画の読み込みを共通化する
    abstract class VideoStreamReader : IDisposable
    {
        protected int ready = 0;

        public abstract int Read(byte[] buf);
        public abstract void Dispose();
        public bool Ready { get { return ready > 0; } } 
    }

    class LiveStreamReader : VideoStreamReader
    {
        TvServer server;
        LiveStream stream;

        public LiveStreamReader(string tunerName, long fsid)
        {
            try
            {
                SleepState.Stop(true);
                Tuner tuner;
                Service service;

                using (var tvdb = new Tvdb(true))
                {
                    tuner = new Tuner(tvdb, tunerName);
                    service = new Service(tvdb, fsid);
                }

                server = new TvServer(tuner);
                server.Open(false);
                server.AddRef();
                server.SetService(service);

                stream = new LiveStream("/tvmaid/shared/stream/" + tuner.DriverId, fsid);

                Interlocked.Increment(ref ready);
            }
            catch
            {
                Dispose();
                throw;
            }
        }
        
        public override int Read(byte[] buf)
        {
            if (server.IsOpen() == false)
                throw new Exception("TVTestが終了しました。");

            var watch = new Stopwatch();
            watch.Start();
            const int timeout = 20 * 1000;   //最大待ち時間

            while (true)
            {
                var count = stream.Read(buf);
                if (count > 0)
                    return count;
                else if (watch.ElapsedMilliseconds > timeout)
                    return 0;
                else
                    Thread.Sleep(50);
            }
        }

        public override void Dispose()
        {
            Interlocked.Decrement(ref ready);

            try
            {
                if (stream != null)
                    stream.Dispose();

                Thread.Sleep(2 * 1000);    //終了を遅らせる(すぐに次のリクエストがあるとTVTextが終了中になるため)

                if (server != null)
                {
                    server.RemoveRef();
                    server.Close();
                }
            }
            catch { }

            SleepState.Stop(false);
        }
    }

    class RecordStreamReader : VideoStreamReader
    {
        FileStream stream;
        double duration;

        public RecordStreamReader(int id, int start, string req = "") // "録画"でサブフォルダと"ライブラリ"対応
        {
            /*
            Record rec;

            using (var tvdb = new Tvdb(true))
                rec = new Record(tvdb, id);

            var path = Path.Combine(AppDefine.Main.Data["record.folder"], rec.File);

            if (File.Exists(path) == false)
                throw new Exception("指定されたIDの録画ファイルはありません。");

            duration = (rec.End - rec.Start).TotalSeconds;
            */

            SleepState.Stop(true);

            var path = "";

            if (req == "")
            {
                Record rec;
                using (var tvdb = new Tvdb(true))
                    rec = new Record(tvdb, id);

                // 録画
                List<string> recFolder = Util.GetRecPath();
                for (int i = 0; i < recFolder.Count; i++)
                {
                    //IEnumerable<string> files = Directory.EnumerateFiles(recFolder[i], rec.File, SearchOption.AllDirectories);
                    List<string> files = FindFile.SearchFile(recFolder[i], rec.File);
                    foreach (var file in files)
                    {
                        if (file.Length > 0)
                        {
                            path = file;
                            duration = (rec.End - rec.Start).TotalSeconds;
                            break;
                        }
                        else
                            path = "";
                    }
                }
            }
            else
            {
                MediaLib mlib;
                using (var libdb = new Libdb(true))
                    mlib = new MediaLib(libdb, id);

                // ライブラリ
                if (File.Exists(mlib.FilePath))
                {
                    path = mlib.FilePath;
                    duration = mlib.Duration;
                }
                else
                    path = "";
            }

            if (path == "")
            {
                SleepState.Stop(false);
                throw new Exception("指定されたIDの録画ファイルはありません。");
            }
            if (start < 0 || start > duration)
            {
                SleepState.Stop(false);
                throw new Exception("record streamで無効な開始時間が指定されました。");
            }

            stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

            if (Path.GetExtension(path) == ".ts")
            {
                var pos = (long)Math.Floor(stream.Length / duration) * start;
                pos -= (pos % 188);
                stream.Seek(pos, SeekOrigin.Begin);
            }

            Interlocked.Increment(ref ready);
        }

        public override int Read(byte[] buf)
        {
            return stream.Read(buf, 0, buf.Length);
        }

        public override void Dispose()
        {
            Interlocked.Decrement(ref ready);

            try
            {
                stream.Dispose();
            }
            catch { }

            SleepState.Stop(false);
        }
    }
}
