﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using MediaInfo.DotNetWrapper.Enumerations;

namespace Tvmaid
{
    class MediaLib
    {
        //static Libdb libdb;

        public int Id = -1;             //-1: 新規
        // file info
        public string FilePath;
        public string File;
        public DateTime FileCreateDate;
        public DateTime FileModifiedDate;
        public long FileSize;
        public int Duration;            // duration
        // id3 tags
        public int MediaType = 0;      // iTunes Media Type { 1:Normal(Music), 2:Audiobook, 6:Music Video, 9:Movie, 10:TV Show, 11:Booklet, 14:Ringtone }
        public string TitleGroup = "";  // 
        public string Title;            // show & album
        public string SubTitle = "";    // subtitle
        public string ServiceName = ""; // artist
        public string Desc = "";        // Comment
        public string LongDesc = "";    // Description
        public int Season = 10;         // Season number  x10して格納
        public int Episode = 0;         // track & episode_sort  x10して格納(n.5話対策)
        public string GenreText = "";   // Genre
        public string Yomi = "";        // yomi
        public string Roma = "";        // roma
        public DateTime AiredDate;      // date
        // extra info
        public DateTime? PlayDate = null;       // last play datetime
        public int PlayCount = 0;       // count of play
        public int Rate = 0;            // rate(count of ★)
        public string Remarks = "";     // 手書きコメント
        public int IsAlive = 0;         // 0:ok 1:not file found?
        public int IsCheck = 0;         // 

        public MediaLib() { }

        public MediaLib(DbTable t)
        {
            Init(t);
        }

        void Init(DbTable t)
        {
            Id = t.GetInt("id");

            FilePath = t.GetStr("path");
            File = t.GetStr("file");
            FileCreateDate = new DateTime(t.GetLong("file_create"));
            FileModifiedDate = new DateTime(t.GetLong("file_modified"));
            FileSize = t.GetLong("file_size");
            Duration = t.GetInt("duration");

            MediaType = t.GetInt("media_type");
            TitleGroup = t.GetStr("title_group");
            Title = t.GetStr("title");
            SubTitle = t.GetStr("subtitle");
            ServiceName = t.GetStr("service");
            Desc = t.GetStr("comment");
            LongDesc = t.GetStr("description");
            Season = t.GetInt("season");

            Episode = t.GetInt("episode");
            GenreText = t.GetStr("genre");
            Yomi = t.GetStr("yomi");
            Roma = t.GetStr("roma");
            AiredDate = new DateTime(t.GetLong("aired_date"));

            PlayDate = new DateTime(t.GetLong("play_date"));
            PlayCount = t.GetInt("play_count");
            Rate = t.GetInt("rate");
            Remarks = t.GetStr("remarks");
            IsAlive = t.GetInt("is_alive");
            IsCheck = t.GetInt("is_check");
        }

        public MediaLib(Libdb libd, int id)
        {
            libd.Sql = "select * from library where id = " + id;
            using (var t = libd.GetTable())
            {
                if (t.Read())
                    Init(t);
                else
                    throw new Exception("ライブラリ情報が見つかりません。");
            }
        }

        // AddRecord // 1レコード挿入
        public void Add(Libdb libdb)
        {
            try
            {
                libdb.BeginTrans();

                if (Id == -1)
                    Id = libdb.GetNextId("library");

                libdb.Sql = @"insert into [library](
                            id, path, file, file_create, file_modified, file_size, duration,
                            media_type, title_group, title, subtitle, service, [comment], description, season, episode, genre, yomi, roma, aired_date,
                            remarks, create_at
                            ) 
                            values(
                            {0}, '{1}', '{2}', {3}, {4}, {5}, {6},
                            {7}, '{21}', '{8}', '{9}', '{10}', '{11}', '{12}',{13}, {14}, '{15}', '{16}', '{17}', {18},
                            '{19}', {20});".Formatex(
                                Id,
                                Libdb.SqlEncode(FilePath),
                                Libdb.SqlEncode(File),
                                FileCreateDate.Ticks,
                                FileModifiedDate.Ticks,
                                FileSize,
                                Duration,

                                0, // 7
                                Libdb.SqlEncode(Title),
                                Libdb.SqlEncode(SubTitle),
                                Libdb.SqlEncode(ServiceName), //10
                                Libdb.SqlEncode(Desc),
                                Libdb.SqlEncode(LongDesc),
                                Season,
                                Episode,
                                Libdb.SqlEncode(GenreText), // 15
                                Libdb.SqlEncode(Yomi),
                                Libdb.SqlEncode(Roma),
                                AiredDate.Ticks, // 18

                                Libdb.SqlEncode(Remarks),
                                DateTime.Now.Ticks, // 20 create_at

                                Libdb.SqlEncode(TitleGroup) // 21

                                );
                libdb.Execute();
            }
            finally
            {
                libdb.Commit();
            }

        }

        // DelRecord // 1レコード削除
        public void Delete(Libdb libdb, int id)
        {
            lock (libdb)
            {
                libdb.Sql = @"delete from library where id={0}".Formatex(id);
                libdb.Execute();
            }
        }

        // 以下はUtil.csっぽいなにか…

        // (主に)ファイル名からタイトルを取得する
        public string GetShowName(string filename)
        {
            Regex regex = new Regex(@"第\d+[話回]|#\d+");
            string[] z = regex.Split(filename);

            string x = "";
            x = z[0].Trim();

            return x;
        }

        // (主に)ファイル名から話数を取得する
        public int GetEpisodeNo(string filename)
        {
            Regex regex = new Regex(@"第(\d+\.*\d*)[話回]|#(\d+\.*\d*)", RegexOptions.IgnoreCase);
            var z = regex.Match(filename);
            if (z.Success)
            {
                return (int)(Strings.StrConv(z.Groups[1].Value != "" ? z.Groups[1].Value : z.Groups[2].Value, VbStrConv.Narrow, 0).ToDouble() * 10);
            }
            return 0;
        }

        // (主に)ファイル名からサブタイトルを取得する
        public string GetSubTitle(string filename)
        {
            Regex regex = new Regex(@".+[「『](.*)[』」]", RegexOptions.IgnoreCase);
            var z = regex.Match(filename);

            string x = "";
            if (z.Success)
            {
                x = z.Groups[1].Value;
            }
            else
            {
                regex = new Regex(@"第(\d+)[話回]|#(\d+)"); //話数の後をサブタイトル扱い
                string[] y = regex.Split(filename);
                if (y.Length == 3)
                    x = y[2].Trim(); // 
            }

            return x;
        }
        
        // 全角→半角で使用
        string Zenhan(Match m)
        {
            return Strings.StrConv(m.Value, VbStrConv.Narrow, 0);
        }

        // 特定ファイルパス構造時の処理(TitleGroup)
        public void Path2Title(string path)
        {
            /* 想定しているフォルダ構成
            ├ "タイトル A"    
            │  ├ season 1
            │  │  ├ "タイトル 第01話 「サブタイトル」.m4v"
            │  │  ├     ：
            │  │
            │  ├ season 2
            │  │  ├ "タイトルII 第01話 「サブタイトル」.m4v"
            │  │  ├     ：
            │  │
            │  ├ OVA1.5 ...season 1.5になります
            │  │  ├ "OVAタイトル 第01話 「サブタイトル」.m4v"
            │  │  └ "OVAタイトル 第02話 「サブタイトル」.m4v"
            │  ├ OVA2タイトル.m4v ...seasonは１になります。
            │  └ 劇場版 タイトル -サブタイトル-.m4v
            │
            ├ "タイトル B"    
            │  ├ "タイトルB 第01話 「サブタイトル」.m4v"
            │  ├     ：
            │ 
            */

            // Fileがあるフォルダが"Season n"の場合
            DirectoryInfo oya = Directory.GetParent(path);
            Regex regex = new Regex(@"season\s*(\d+\.*\d*)|シーズン\s*(\d+\.*\d*)", RegexOptions.IgnoreCase);
            var z = regex.Match(oya.Name);
            if (z.Success)
            {
                Season = (int)(Strings.StrConv(z.Groups[1].Value != "" ? z.Groups[1].Value : z.Groups[2].Value, VbStrConv.Narrow, 0).ToDouble() * 10);
                TitleGroup = oya.Parent.Name;
                return;
            }

            // Fileがあるフォルダにseasonフォルダが有る場合
            DirectoryInfo di = new DirectoryInfo(oya.FullName);
            DirectoryInfo[] subFolders = di.GetDirectories("*"); // SearchOption.AllDirectoriesを外した(mod14.2)
            foreach (System.IO.DirectoryInfo subFolder in subFolders)
            {
                z = regex.Match(subFolder.Name);
                if (z.Success)
                {
                    // season居た！
                    TitleGroup = subFolder.Parent.Name;
                    return;
                }
            }

            // Fileがあるフォルダが"OVA"の場合(OVA1, OVA2...数字が "Season" になります)
            regex = new Regex(@"OVA\s*(\d*\.*\d*)", RegexOptions.IgnoreCase);
            z = regex.Match(oya.Name);
            if (z.Success)
            {
                if(z.Groups[1].Value != "")
                    Season = (int)(Strings.StrConv(z.Groups[1].Value != "" ? z.Groups[1].Value : "0", VbStrConv.Narrow, 0).ToDouble() * 10);

                TitleGroup = oya.Parent.Name; // Title
                return;
            }

            // 親フォルダにファイル名の一部を含むものがあれば，それを使う
            string fn = Path.GetFileNameWithoutExtension(path);
            DirectoryInfo d = new DirectoryInfo(path);
            do
            {
                string pn = d.Parent.Name;
                regex = new Regex(Regex.Escape(pn), RegexOptions.IgnoreCase);
                z = regex.Match(fn);
                if (z.Success)
                {
                    TitleGroup = pn; // Title
                    break;
                }
                d = d.Parent;
            } while (d.Parent != null);

            // ファイルが格納されている上位フォルダに"Season"や"OVA"があれば，それを使う(直上でなくてもOK)
            d = new DirectoryInfo(path);
            regex = new Regex(@"season\s*(\d+\.*\d*)|シーズン\s*(\d+\.*\d*)|OVA\s*(\d*\.*\d*)", RegexOptions.IgnoreCase);
            do
            {
                z = regex.Match(d.Parent.Name);
                if (z.Success)
                {
                    Season = (int)(Strings.StrConv(z.Groups[1].Value != "" ? z.Groups[1].Value : z.Groups[2].Value, VbStrConv.Narrow, 0).ToDouble() * 10);
                    break;
                }
                d = d.Parent;
            } while (d.Parent != null);
        }
    }

    public class Libdb : Database
    {
        protected override string Path
        {
            get { return "library.db"; }
        }

        public Libdb() { }
        public Libdb(bool open) : base(open) { }
    }
    
    class Mp4FileInfo
    {
        // base TsFileInfo
        public DateTime StartTime;
        public DateTime EndTime;
        public DateTime ReserveStart;
        public DateTime ReserveEnd;

        public string Title = "";
        public string Service = "";
        public string Desc = "";
        public string LongDesc = "";
        public string Genre = "";
        public string File;

        // id3 tags
        //public int MediaType = 0;      // iTunes Media Type { 1:Normal(Music), 2:Audiobook, 6:Music Video, 9:Movie, 10:TV Show, 11:Booklet, 14:Ringtone }
        //public string ShowName;        // show & album = Titile
        public string Subtitle = "";    // subtitle
        //public string ServiceName = ""; // artist
        //public string Desc = "";        // Comment
        //public string LongDesc = "";    // Description
        public int Season = 10;         // Season number(x10)
        public int Episode = 0;         // track & episode_sort(x10)
        //public string GenreText = "";   // =>Genre
        public string Yomi;             // yomi
        public string Roma;             // roma
        public DateTime AiredDate;      // date
        
        public Mp4FileInfo(string path) // path is full path
        {
            MediaLib mlib = new MediaLib();

            File = Path.GetFileName(path);
            Title = Path.GetFileNameWithoutExtension(path); //仮置き

            using (var mediaInfo = new MediaInfo.DotNetWrapper.MediaInfo())
            {
                // "mediaInfo.Get" parameters, check "MediaInfo.exe --Info-Parameters" ...1500 lines output! //

                mediaInfo.Open(path); // open mp4 file
                //var IsStreamable = mediaInfo.Get(StreamKind.General, 0, "IsStreamable");
                //MediaType = mediaInfo.Get(StreamKind.General, 0, "ContentType");

                // ACv3改ネタからTitle再構成
                Subtitle = mediaInfo.Get(StreamKind.General, 0, "Title");

                Service = mediaInfo.Get(StreamKind.General, 0, "Performer");

                Desc = mediaInfo.Get(StreamKind.General, 0, "Comment");
                if (Desc == "")
                    Desc = mediaInfo.Get(StreamKind.General, 0, "Album");

                LongDesc = mediaInfo.Get(StreamKind.General, 0, "Description");
                
                var se = mediaInfo.Get(StreamKind.General, 0, "Season_Position");
                Season = se == "" ? 10 : se.ToInt() * 10;

                var ep = mediaInfo.Get(StreamKind.General, 0, "Track/Position");
                Episode = ep == "" ? 0 : ep.ToInt() * 10;
                
                Genre = (mediaInfo.Get(StreamKind.General, 0, "Genre") == "" ? "その他" : mediaInfo.Get(StreamKind.General, 0, "Genre"));


                var rs = mediaInfo.Get(StreamKind.General, 0, "File_Modified_Date_Local");
                ReserveStart = StartTime = DateTime.Parse(rs);
                ReserveEnd = EndTime = StartTime.AddMilliseconds(mediaInfo.Get(StreamKind.General, 0, "Duration").ToDouble());

                var ad = mediaInfo.Get(StreamKind.General, 0, "Recorded_Date");
                if (ad != "")
                {
                    var h = 0;
                    if (ad.Length == 4) 
                        ad = ad + "-01-01";

                    Regex regex = new Regex(@"UTC\s*", RegexOptions.IgnoreCase);
                    var z = regex.Match(ad);
                    if (z.Success)
                    {
                        ad = regex.Replace(ad, "");
                        h = 9;
                    }

                    AiredDate = DateTime.Parse(ad).AddHours(h);
                }
                //var x = new MsIme.IFELang();
                Yomi = ""; // x.GetYomi(Title);
                //x.Dispose();

                Roma = "";

                // Title再構成
                string ac3Title = "";
                ac3Title += mediaInfo.Get(StreamKind.General, 0, "Collection");
                if (ac3Title == "")
                    ac3Title += mediaInfo.Get(StreamKind.General, 0, "Album");

                if (ac3Title != "")
                {
                    //ac3Title = ac3Title + " " + Subtitle;
                    Title = ac3Title;
                }

                mediaInfo.Close();

            }
        }
    }
}