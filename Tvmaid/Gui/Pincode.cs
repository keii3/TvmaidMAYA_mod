﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tvmaid.Gui
{
    public partial class Pincode : Form
    {
        //ユーザ入力値
        public string ResultText = "";

        public Pincode()
        {
            InitializeComponent();
        }

        private void twitterAcceptButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            //ユーザ入力値を格納
            ResultText = twitterPincodeBox.Text;
            this.Close();

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
