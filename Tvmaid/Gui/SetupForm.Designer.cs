﻿namespace Tvmaid
{
    partial class SetupForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.acceptButton = new System.Windows.Forms.Button();
            this.tvtestDialog = new System.Windows.Forms.SaveFileDialog();
            this.recDirDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.driverDialog = new System.Windows.Forms.SaveFileDialog();
            this.postProcessDialog = new System.Windows.Forms.SaveFileDialog();
            this.cancelButton = new System.Windows.Forms.Button();
            this.otherTabPage = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.recFileRemoveBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.autoSleepIntervalBox = new System.Windows.Forms.TextBox();
            this.epgHourBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.epgBasicC2Check = new System.Windows.Forms.CheckBox();
            this.epgBasicC1Check = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.epgBasicBCheck = new System.Windows.Forms.CheckBox();
            this.postProcessForceCheck = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.postProcessArgBox = new System.Windows.Forms.TextBox();
            this.postProcessBox = new System.Windows.Forms.TextBox();
            this.recFileBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.autoSleepCheck = new System.Windows.Forms.CheckBox();
            this.postProcessRefButton = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tunerTabPage = new System.Windows.Forms.TabPage();
            this.tunerPanel = new System.Windows.Forms.Panel();
            this.tunerBox = new System.Windows.Forms.TreeView();
            this.tunerNameBox = new System.Windows.Forms.TextBox();
            this.upButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.driverRefButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.removeButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.driverBox = new System.Windows.Forms.TextBox();
            this.tunerAddButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tunerUpdateCheck = new System.Windows.Forms.CheckBox();
            this.baseicTabPage = new System.Windows.Forms.TabPage();
            this.recSubFolderCheck = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tvtestRecordWaitBox = new System.Windows.Forms.TextBox();
            this.tvtestServiceWaitBox = new System.Windows.Forms.TextBox();
            this.recMarginEndBox = new System.Windows.Forms.TextBox();
            this.recMarginStartBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.recDirSpareRefButton = new System.Windows.Forms.Button();
            this.recDirSpareBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.unregStartupButton = new System.Windows.Forms.Button();
            this.regStartupButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.recDirRefButton = new System.Windows.Forms.Button();
            this.tvtestRefButton = new System.Windows.Forms.Button();
            this.recDirBox = new System.Windows.Forms.TextBox();
            this.tvtestBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.other2TabPage = new System.Windows.Forms.TabPage();
            this.thumnailPanel = new System.Windows.Forms.Panel();
            this.thumbnailStartMarginBox = new System.Windows.Forms.TextBox();
            this.thumbnailDelayBox = new System.Windows.Forms.TextBox();
            this.thumbnailModeComboBox = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.thumbnailEnableCheck = new System.Windows.Forms.CheckBox();
            this.chatTabPage = new System.Windows.Forms.TabPage();
            this.label34 = new System.Windows.Forms.Label();
            this.niconicoPasswordBox = new System.Windows.Forms.TextBox();
            this.niconicoMailBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tweetTabPage = new System.Windows.Forms.TabPage();
            this.gCalenerPanel = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.gCalenderIdBox = new System.Windows.Forms.TextBox();
            this.gCalenderEnableCheck = new System.Windows.Forms.CheckBox();
            this.twitterPanel = new System.Windows.Forms.Panel();
            this.tweetTestButton = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.twitterapi_secretBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.twitterapi_keyBox = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.twitterStartEnableCheck = new System.Windows.Forms.CheckBox();
            this.twitterMessageStartBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.twitterEndEnableCheck = new System.Windows.Forms.CheckBox();
            this.twitterSleepEnableCheck = new System.Windows.Forms.CheckBox();
            this.twitterMessageEndBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.twitterAcceptButton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.twitterUserBox = new System.Windows.Forms.TextBox();
            this.twitterEnableCheck = new System.Windows.Forms.CheckBox();
            this.libraryTabPage = new System.Windows.Forms.TabPage();
            this.label40 = new System.Windows.Forms.Label();
            this.libAddButton = new System.Windows.Forms.Button();
            this.libDelButton = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.libDownButton = new System.Windows.Forms.Button();
            this.libUpButton = new System.Windows.Forms.Button();
            this.libView = new System.Windows.Forms.TreeView();
            this.libButton1 = new System.Windows.Forms.Button();
            this.libBox = new System.Windows.Forms.TextBox();
            this.libDirDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.label45 = new System.Windows.Forms.Label();
            this.otherTabPage.SuspendLayout();
            this.tunerTabPage.SuspendLayout();
            this.tunerPanel.SuspendLayout();
            this.baseicTabPage.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.other2TabPage.SuspendLayout();
            this.thumnailPanel.SuspendLayout();
            this.chatTabPage.SuspendLayout();
            this.tweetTabPage.SuspendLayout();
            this.gCalenerPanel.SuspendLayout();
            this.twitterPanel.SuspendLayout();
            this.libraryTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(351, 393);
            this.acceptButton.Margin = new System.Windows.Forms.Padding(2);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(91, 24);
            this.acceptButton.TabIndex = 1;
            this.acceptButton.Text = "OK";
            this.acceptButton.Click += new System.EventHandler(this.endButton_Click);
            // 
            // tvtestDialog
            // 
            this.tvtestDialog.CheckFileExists = true;
            this.tvtestDialog.Filter = "TVTest|TVTest.exe";
            this.tvtestDialog.OverwritePrompt = false;
            this.tvtestDialog.Title = "TVTestの場所";
            // 
            // recDirDialog
            // 
            this.recDirDialog.Description = "録画フォルダを選択してください。";
            this.recDirDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // driverDialog
            // 
            this.driverDialog.CheckFileExists = true;
            this.driverDialog.Filter = "BonDriver|Bondriver*.dll";
            this.driverDialog.OverwritePrompt = false;
            this.driverDialog.Title = "Bonドライバ";
            // 
            // postProcessDialog
            // 
            this.postProcessDialog.CheckFileExists = true;
            this.postProcessDialog.Filter = "実行ファイル (*.exe *.bat)|*.exe;*.bat";
            this.postProcessDialog.OverwritePrompt = false;
            this.postProcessDialog.Title = "録画後プロセス";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(447, 393);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(91, 24);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "キャンセル";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // otherTabPage
            // 
            this.otherTabPage.Controls.Add(this.label29);
            this.otherTabPage.Controls.Add(this.recFileRemoveBox);
            this.otherTabPage.Controls.Add(this.label28);
            this.otherTabPage.Controls.Add(this.autoSleepIntervalBox);
            this.otherTabPage.Controls.Add(this.epgHourBox);
            this.otherTabPage.Controls.Add(this.label26);
            this.otherTabPage.Controls.Add(this.epgBasicC2Check);
            this.otherTabPage.Controls.Add(this.epgBasicC1Check);
            this.otherTabPage.Controls.Add(this.label25);
            this.otherTabPage.Controls.Add(this.epgBasicBCheck);
            this.otherTabPage.Controls.Add(this.postProcessForceCheck);
            this.otherTabPage.Controls.Add(this.label15);
            this.otherTabPage.Controls.Add(this.label11);
            this.otherTabPage.Controls.Add(this.postProcessArgBox);
            this.otherTabPage.Controls.Add(this.postProcessBox);
            this.otherTabPage.Controls.Add(this.recFileBox);
            this.otherTabPage.Controls.Add(this.label8);
            this.otherTabPage.Controls.Add(this.label17);
            this.otherTabPage.Controls.Add(this.label14);
            this.otherTabPage.Controls.Add(this.autoSleepCheck);
            this.otherTabPage.Controls.Add(this.postProcessRefButton);
            this.otherTabPage.Controls.Add(this.label13);
            this.otherTabPage.Controls.Add(this.label12);
            this.otherTabPage.Controls.Add(this.label27);
            this.otherTabPage.Location = new System.Drawing.Point(4, 24);
            this.otherTabPage.Margin = new System.Windows.Forms.Padding(2);
            this.otherTabPage.Name = "otherTabPage";
            this.otherTabPage.Padding = new System.Windows.Forms.Padding(2);
            this.otherTabPage.Size = new System.Drawing.Size(521, 350);
            this.otherTabPage.TabIndex = 3;
            this.otherTabPage.Text = "その他";
            this.otherTabPage.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(126, 82);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(365, 15);
            this.label29.TabIndex = 4;
            this.label29.Text = "{title}から記述した文字を取り除きます。(カンマで区切って複数指定可能)";
            // 
            // recFileRemoveBox
            // 
            this.recFileRemoveBox.Location = new System.Drawing.Point(129, 57);
            this.recFileRemoveBox.Margin = new System.Windows.Forms.Padding(2);
            this.recFileRemoveBox.Name = "recFileRemoveBox";
            this.recFileRemoveBox.Size = new System.Drawing.Size(370, 23);
            this.recFileRemoveBox.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(60, 60);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 15);
            this.label28.TabIndex = 2;
            this.label28.Text = "除外文字";
            // 
            // autoSleepIntervalBox
            // 
            this.autoSleepIntervalBox.Location = new System.Drawing.Point(296, 266);
            this.autoSleepIntervalBox.Margin = new System.Windows.Forms.Padding(2);
            this.autoSleepIntervalBox.Name = "autoSleepIntervalBox";
            this.autoSleepIntervalBox.Size = new System.Drawing.Size(40, 23);
            this.autoSleepIntervalBox.TabIndex = 22;
            // 
            // epgHourBox
            // 
            this.epgHourBox.Location = new System.Drawing.Point(185, 207);
            this.epgHourBox.Margin = new System.Windows.Forms.Padding(2);
            this.epgHourBox.Name = "epgHourBox";
            this.epgHourBox.Size = new System.Drawing.Size(108, 23);
            this.epgHourBox.TabIndex = 14;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(301, 210);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 15);
            this.label26.TabIndex = 15;
            this.label26.Text = "詳細取得:";
            // 
            // epgBasicC2Check
            // 
            this.epgBasicC2Check.AutoSize = true;
            this.epgBasicC2Check.Location = new System.Drawing.Point(457, 209);
            this.epgBasicC2Check.Margin = new System.Windows.Forms.Padding(2);
            this.epgBasicC2Check.Name = "epgBasicC2Check";
            this.epgBasicC2Check.Size = new System.Drawing.Size(49, 19);
            this.epgBasicC2Check.TabIndex = 18;
            this.epgBasicC2Check.Text = "CS2";
            this.epgBasicC2Check.UseVisualStyleBackColor = true;
            // 
            // epgBasicC1Check
            // 
            this.epgBasicC1Check.AutoSize = true;
            this.epgBasicC1Check.Location = new System.Drawing.Point(408, 209);
            this.epgBasicC1Check.Margin = new System.Windows.Forms.Padding(2);
            this.epgBasicC1Check.Name = "epgBasicC1Check";
            this.epgBasicC1Check.Size = new System.Drawing.Size(49, 19);
            this.epgBasicC1Check.TabIndex = 17;
            this.epgBasicC1Check.Text = "CS1";
            this.epgBasicC1Check.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(126, 210);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 15);
            this.label25.TabIndex = 13;
            this.label25.Text = "更新時刻:";
            // 
            // epgBasicBCheck
            // 
            this.epgBasicBCheck.AutoSize = true;
            this.epgBasicBCheck.Location = new System.Drawing.Point(364, 209);
            this.epgBasicBCheck.Margin = new System.Windows.Forms.Padding(2);
            this.epgBasicBCheck.Name = "epgBasicBCheck";
            this.epgBasicBCheck.Size = new System.Drawing.Size(42, 19);
            this.epgBasicBCheck.TabIndex = 16;
            this.epgBasicBCheck.Text = "BS";
            this.epgBasicBCheck.UseVisualStyleBackColor = true;
            // 
            // postProcessForceCheck
            // 
            this.postProcessForceCheck.AutoSize = true;
            this.postProcessForceCheck.Location = new System.Drawing.Point(129, 178);
            this.postProcessForceCheck.Margin = new System.Windows.Forms.Padding(2);
            this.postProcessForceCheck.Name = "postProcessForceCheck";
            this.postProcessForceCheck.Size = new System.Drawing.Size(245, 19);
            this.postProcessForceCheck.TabIndex = 11;
            this.postProcessForceCheck.Text = "強制的に実行 (録画に失敗しても実行します)";
            this.postProcessForceCheck.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(126, 159);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(275, 15);
            this.label15.TabIndex = 10;
            this.label15.Text = "引数空欄で，録画ファイル名(Full Path)が渡されます。";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(84, 137);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 15);
            this.label11.TabIndex = 8;
            this.label11.Text = "引数";
            // 
            // postProcessArgBox
            // 
            this.postProcessArgBox.Location = new System.Drawing.Point(129, 134);
            this.postProcessArgBox.Margin = new System.Windows.Forms.Padding(2);
            this.postProcessArgBox.Name = "postProcessArgBox";
            this.postProcessArgBox.Size = new System.Drawing.Size(370, 23);
            this.postProcessArgBox.TabIndex = 9;
            this.postProcessArgBox.TextChanged += new System.EventHandler(this.postProcessBoxExt_TextChanged);
            // 
            // postProcessBox
            // 
            this.postProcessBox.Location = new System.Drawing.Point(129, 105);
            this.postProcessBox.Margin = new System.Windows.Forms.Padding(2);
            this.postProcessBox.Name = "postProcessBox";
            this.postProcessBox.Size = new System.Drawing.Size(274, 23);
            this.postProcessBox.TabIndex = 6;
            // 
            // recFileBox
            // 
            this.recFileBox.Location = new System.Drawing.Point(129, 29);
            this.recFileBox.Margin = new System.Windows.Forms.Padding(2);
            this.recFileBox.Name = "recFileBox";
            this.recFileBox.Size = new System.Drawing.Size(370, 23);
            this.recFileBox.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(53, 291);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(365, 33);
            this.label8.TabIndex = 23;
            this.label8.Text = "スリープからの自動復帰時、録画終了後にスリープ状態に戻す機能です。\r\n（録画後プロセスが指定されているときは無視され、OFFになります）\r\n";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(126, 233);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(256, 15);
            this.label17.TabIndex = 19;
            this.label17.Text = "カンマで区切って、複数指定できます。(例) 9,15,21";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(48, 210);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 12;
            this.label14.Text = "番組表更新";
            // 
            // autoSleepCheck
            // 
            this.autoSleepCheck.AutoSize = true;
            this.autoSleepCheck.Location = new System.Drawing.Point(39, 268);
            this.autoSleepCheck.Margin = new System.Windows.Forms.Padding(2);
            this.autoSleepCheck.Name = "autoSleepCheck";
            this.autoSleepCheck.Size = new System.Drawing.Size(114, 19);
            this.autoSleepCheck.TabIndex = 20;
            this.autoSleepCheck.Text = "自動スリープを行う\r\n";
            this.autoSleepCheck.UseVisualStyleBackColor = true;
            // 
            // postProcessRefButton
            // 
            this.postProcessRefButton.Location = new System.Drawing.Point(406, 105);
            this.postProcessRefButton.Margin = new System.Windows.Forms.Padding(2);
            this.postProcessRefButton.Name = "postProcessRefButton";
            this.postProcessRefButton.Size = new System.Drawing.Size(91, 24);
            this.postProcessRefButton.TabIndex = 7;
            this.postProcessRefButton.Text = "参照...";
            this.postProcessRefButton.UseVisualStyleBackColor = true;
            this.postProcessRefButton.Click += new System.EventHandler(this.postProcessRefButton_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(35, 108);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 15);
            this.label13.TabIndex = 5;
            this.label13.Text = "録画後プロセス";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(38, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "録画ファイル名";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(175, 269);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(122, 15);
            this.label27.TabIndex = 21;
            this.label27.Text = "スリープまでの時間(分):";
            // 
            // tunerTabPage
            // 
            this.tunerTabPage.Controls.Add(this.tunerPanel);
            this.tunerTabPage.Controls.Add(this.tunerUpdateCheck);
            this.tunerTabPage.Location = new System.Drawing.Point(4, 24);
            this.tunerTabPage.Margin = new System.Windows.Forms.Padding(2);
            this.tunerTabPage.Name = "tunerTabPage";
            this.tunerTabPage.Padding = new System.Windows.Forms.Padding(2);
            this.tunerTabPage.Size = new System.Drawing.Size(521, 350);
            this.tunerTabPage.TabIndex = 1;
            this.tunerTabPage.Text = "チューナ";
            this.tunerTabPage.UseVisualStyleBackColor = true;
            // 
            // tunerPanel
            // 
            this.tunerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tunerPanel.Controls.Add(this.tunerBox);
            this.tunerPanel.Controls.Add(this.tunerNameBox);
            this.tunerPanel.Controls.Add(this.upButton);
            this.tunerPanel.Controls.Add(this.downButton);
            this.tunerPanel.Controls.Add(this.driverRefButton);
            this.tunerPanel.Controls.Add(this.label7);
            this.tunerPanel.Controls.Add(this.removeButton);
            this.tunerPanel.Controls.Add(this.label6);
            this.tunerPanel.Controls.Add(this.label5);
            this.tunerPanel.Controls.Add(this.driverBox);
            this.tunerPanel.Controls.Add(this.tunerAddButton);
            this.tunerPanel.Controls.Add(this.label2);
            this.tunerPanel.Enabled = false;
            this.tunerPanel.Location = new System.Drawing.Point(5, 38);
            this.tunerPanel.Margin = new System.Windows.Forms.Padding(2);
            this.tunerPanel.Name = "tunerPanel";
            this.tunerPanel.Size = new System.Drawing.Size(512, 306);
            this.tunerPanel.TabIndex = 1;
            // 
            // tunerBox
            // 
            this.tunerBox.FullRowSelect = true;
            this.tunerBox.HideSelection = false;
            this.tunerBox.Indent = 5;
            this.tunerBox.Location = new System.Drawing.Point(96, 123);
            this.tunerBox.Margin = new System.Windows.Forms.Padding(2);
            this.tunerBox.Name = "tunerBox";
            this.tunerBox.ShowLines = false;
            this.tunerBox.ShowNodeToolTips = true;
            this.tunerBox.ShowPlusMinus = false;
            this.tunerBox.ShowRootLines = false;
            this.tunerBox.Size = new System.Drawing.Size(305, 162);
            this.tunerBox.TabIndex = 8;
            // 
            // tunerNameBox
            // 
            this.tunerNameBox.Location = new System.Drawing.Point(96, 85);
            this.tunerNameBox.Margin = new System.Windows.Forms.Padding(2);
            this.tunerNameBox.Name = "tunerNameBox";
            this.tunerNameBox.Size = new System.Drawing.Size(305, 23);
            this.tunerNameBox.TabIndex = 5;
            // 
            // upButton
            // 
            this.upButton.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.upButton.Location = new System.Drawing.Point(405, 123);
            this.upButton.Margin = new System.Windows.Forms.Padding(2);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(91, 24);
            this.upButton.TabIndex = 9;
            this.upButton.Text = "上へ";
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // downButton
            // 
            this.downButton.Location = new System.Drawing.Point(405, 160);
            this.downButton.Margin = new System.Windows.Forms.Padding(2);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(91, 24);
            this.downButton.TabIndex = 10;
            this.downButton.Text = "下へ";
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.downButton_Click);
            // 
            // driverRefButton
            // 
            this.driverRefButton.Location = new System.Drawing.Point(405, 48);
            this.driverRefButton.Margin = new System.Windows.Forms.Padding(2);
            this.driverRefButton.Name = "driverRefButton";
            this.driverRefButton.Size = new System.Drawing.Size(91, 24);
            this.driverRefButton.TabIndex = 3;
            this.driverRefButton.Text = "参照...";
            this.driverRefButton.UseVisualStyleBackColor = true;
            this.driverRefButton.Click += new System.EventHandler(this.driverRefButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 123);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 15);
            this.label7.TabIndex = 7;
            this.label7.Text = "チューナ";
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(405, 198);
            this.removeButton.Margin = new System.Windows.Forms.Padding(2);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(91, 24);
            this.removeButton.TabIndex = 11;
            this.removeButton.Text = "削除";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 13);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(311, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Bonドライバ、チューナ名を指定して、追加ボタンを押してください。";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 51);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Bonドライバ";
            // 
            // driverBox
            // 
            this.driverBox.Location = new System.Drawing.Point(96, 49);
            this.driverBox.Margin = new System.Windows.Forms.Padding(2);
            this.driverBox.Name = "driverBox";
            this.driverBox.Size = new System.Drawing.Size(305, 23);
            this.driverBox.TabIndex = 2;
            // 
            // tunerAddButton
            // 
            this.tunerAddButton.Location = new System.Drawing.Point(405, 84);
            this.tunerAddButton.Margin = new System.Windows.Forms.Padding(2);
            this.tunerAddButton.Name = "tunerAddButton";
            this.tunerAddButton.Size = new System.Drawing.Size(91, 24);
            this.tunerAddButton.TabIndex = 6;
            this.tunerAddButton.Text = "追加";
            this.tunerAddButton.UseVisualStyleBackColor = true;
            this.tunerAddButton.Click += new System.EventHandler(this.tunerAddButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 87);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "チューナ名";
            // 
            // tunerUpdateCheck
            // 
            this.tunerUpdateCheck.AutoSize = true;
            this.tunerUpdateCheck.Location = new System.Drawing.Point(21, 12);
            this.tunerUpdateCheck.Margin = new System.Windows.Forms.Padding(2);
            this.tunerUpdateCheck.Name = "tunerUpdateCheck";
            this.tunerUpdateCheck.Size = new System.Drawing.Size(116, 19);
            this.tunerUpdateCheck.TabIndex = 0;
            this.tunerUpdateCheck.Text = "チューナ設定を行う";
            this.tunerUpdateCheck.UseVisualStyleBackColor = true;
            this.tunerUpdateCheck.CheckedChanged += new System.EventHandler(this.tunerUpdateCheck_CheckedChanged);
            // 
            // baseicTabPage
            // 
            this.baseicTabPage.Controls.Add(this.recSubFolderCheck);
            this.baseicTabPage.Controls.Add(this.label24);
            this.baseicTabPage.Controls.Add(this.tvtestRecordWaitBox);
            this.baseicTabPage.Controls.Add(this.tvtestServiceWaitBox);
            this.baseicTabPage.Controls.Add(this.recMarginEndBox);
            this.baseicTabPage.Controls.Add(this.recMarginStartBox);
            this.baseicTabPage.Controls.Add(this.label23);
            this.baseicTabPage.Controls.Add(this.label22);
            this.baseicTabPage.Controls.Add(this.label21);
            this.baseicTabPage.Controls.Add(this.label20);
            this.baseicTabPage.Controls.Add(this.label19);
            this.baseicTabPage.Controls.Add(this.label18);
            this.baseicTabPage.Controls.Add(this.label16);
            this.baseicTabPage.Controls.Add(this.recDirSpareRefButton);
            this.baseicTabPage.Controls.Add(this.recDirSpareBox);
            this.baseicTabPage.Controls.Add(this.label9);
            this.baseicTabPage.Controls.Add(this.unregStartupButton);
            this.baseicTabPage.Controls.Add(this.regStartupButton);
            this.baseicTabPage.Controls.Add(this.label4);
            this.baseicTabPage.Controls.Add(this.recDirRefButton);
            this.baseicTabPage.Controls.Add(this.tvtestRefButton);
            this.baseicTabPage.Controls.Add(this.recDirBox);
            this.baseicTabPage.Controls.Add(this.tvtestBox);
            this.baseicTabPage.Controls.Add(this.label1);
            this.baseicTabPage.Controls.Add(this.label3);
            this.baseicTabPage.Location = new System.Drawing.Point(4, 24);
            this.baseicTabPage.Margin = new System.Windows.Forms.Padding(2);
            this.baseicTabPage.Name = "baseicTabPage";
            this.baseicTabPage.Padding = new System.Windows.Forms.Padding(2);
            this.baseicTabPage.Size = new System.Drawing.Size(521, 350);
            this.baseicTabPage.TabIndex = 0;
            this.baseicTabPage.Text = "基本";
            this.baseicTabPage.UseVisualStyleBackColor = true;
            // 
            // recSubFolderCheck
            // 
            this.recSubFolderCheck.AutoSize = true;
            this.recSubFolderCheck.Location = new System.Drawing.Point(117, 99);
            this.recSubFolderCheck.Name = "recSubFolderCheck";
            this.recSubFolderCheck.Size = new System.Drawing.Size(248, 19);
            this.recSubFolderCheck.TabIndex = 24;
            this.recSubFolderCheck.Text = "\"録画ファイル登録\"は，サブディレクトリも含める";
            this.recSubFolderCheck.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(114, 211);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(241, 15);
            this.label24.TabIndex = 17;
            this.label24.Text = "TvTestに対する，各動作前にウエイトを入れます";
            // 
            // tvtestRecordWaitBox
            // 
            this.tvtestRecordWaitBox.Location = new System.Drawing.Point(308, 184);
            this.tvtestRecordWaitBox.Margin = new System.Windows.Forms.Padding(2);
            this.tvtestRecordWaitBox.Name = "tvtestRecordWaitBox";
            this.tvtestRecordWaitBox.Size = new System.Drawing.Size(50, 23);
            this.tvtestRecordWaitBox.TabIndex = 16;
            // 
            // tvtestServiceWaitBox
            // 
            this.tvtestServiceWaitBox.Location = new System.Drawing.Point(185, 184);
            this.tvtestServiceWaitBox.Margin = new System.Windows.Forms.Padding(2);
            this.tvtestServiceWaitBox.Name = "tvtestServiceWaitBox";
            this.tvtestServiceWaitBox.Size = new System.Drawing.Size(48, 23);
            this.tvtestServiceWaitBox.TabIndex = 14;
            // 
            // recMarginEndBox
            // 
            this.recMarginEndBox.Location = new System.Drawing.Point(241, 127);
            this.recMarginEndBox.Margin = new System.Windows.Forms.Padding(2);
            this.recMarginEndBox.Name = "recMarginEndBox";
            this.recMarginEndBox.Size = new System.Drawing.Size(40, 23);
            this.recMarginEndBox.TabIndex = 10;
            // 
            // recMarginStartBox
            // 
            this.recMarginStartBox.Location = new System.Drawing.Point(152, 127);
            this.recMarginStartBox.Margin = new System.Windows.Forms.Padding(2);
            this.recMarginStartBox.Name = "recMarginStartBox";
            this.recMarginStartBox.Size = new System.Drawing.Size(40, 23);
            this.recMarginStartBox.TabIndex = 8;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(15, 187);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 15);
            this.label23.TabIndex = 12;
            this.label23.Text = "ウエイト設定(ms)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(251, 187);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 15);
            this.label22.TabIndex = 15;
            this.label22.Text = "録画開始:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(116, 187);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 15);
            this.label21.TabIndex = 13;
            this.label21.Text = "サービス切替:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(114, 154);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(237, 15);
            this.label20.TabIndex = 11;
            this.label20.Text = "設定した秒数，録画を早く始め，早く終わります";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(205, 130);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 15);
            this.label19.TabIndex = 9;
            this.label19.Text = "終了:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(116, 130);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 15);
            this.label18.TabIndex = 7;
            this.label18.Text = "開始:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(20, 130);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 15);
            this.label16.TabIndex = 6;
            this.label16.Text = "録画マージン(秒)";
            // 
            // recDirSpareRefButton
            // 
            this.recDirSpareRefButton.Location = new System.Drawing.Point(409, 317);
            this.recDirSpareRefButton.Margin = new System.Windows.Forms.Padding(2);
            this.recDirSpareRefButton.Name = "recDirSpareRefButton";
            this.recDirSpareRefButton.Size = new System.Drawing.Size(91, 24);
            this.recDirSpareRefButton.TabIndex = 20;
            this.recDirSpareRefButton.Text = "参照...";
            this.recDirSpareRefButton.UseVisualStyleBackColor = true;
            this.recDirSpareRefButton.Visible = false;
            this.recDirSpareRefButton.Click += new System.EventHandler(this.recDirSpareRefButton_Click);
            // 
            // recDirSpareBox
            // 
            this.recDirSpareBox.Location = new System.Drawing.Point(117, 317);
            this.recDirSpareBox.Margin = new System.Windows.Forms.Padding(2);
            this.recDirSpareBox.Name = "recDirSpareBox";
            this.recDirSpareBox.Size = new System.Drawing.Size(288, 23);
            this.recDirSpareBox.TabIndex = 19;
            this.recDirSpareBox.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 320);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 15);
            this.label9.TabIndex = 18;
            this.label9.Text = "保存場所(予備)";
            this.label9.Visible = false;
            // 
            // unregStartupButton
            // 
            this.unregStartupButton.Location = new System.Drawing.Point(264, 239);
            this.unregStartupButton.Margin = new System.Windows.Forms.Padding(2);
            this.unregStartupButton.Name = "unregStartupButton";
            this.unregStartupButton.Size = new System.Drawing.Size(140, 24);
            this.unregStartupButton.TabIndex = 22;
            this.unregStartupButton.Text = "登録解除...";
            this.unregStartupButton.UseVisualStyleBackColor = true;
            this.unregStartupButton.Click += new System.EventHandler(this.unregStartupButton_Click);
            // 
            // regStartupButton
            // 
            this.regStartupButton.Location = new System.Drawing.Point(117, 239);
            this.regStartupButton.Margin = new System.Windows.Forms.Padding(2);
            this.regStartupButton.Name = "regStartupButton";
            this.regStartupButton.Size = new System.Drawing.Size(140, 24);
            this.regStartupButton.TabIndex = 21;
            this.regStartupButton.Text = "スタートアップ登録...";
            this.regStartupButton.UseVisualStyleBackColor = true;
            this.regStartupButton.Click += new System.EventHandler(this.regStartupButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.OrangeRed;
            this.label4.Location = new System.Drawing.Point(114, 275);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(278, 30);
            this.label4.TabIndex = 23;
            this.label4.Text = "初めてのセットアップ時は、チューナの設定を行ってください。\r\n上部にタブがあります。";
            // 
            // recDirRefButton
            // 
            this.recDirRefButton.Location = new System.Drawing.Point(409, 67);
            this.recDirRefButton.Margin = new System.Windows.Forms.Padding(2);
            this.recDirRefButton.Name = "recDirRefButton";
            this.recDirRefButton.Size = new System.Drawing.Size(91, 24);
            this.recDirRefButton.TabIndex = 5;
            this.recDirRefButton.Text = "参照...";
            this.recDirRefButton.UseVisualStyleBackColor = true;
            this.recDirRefButton.Click += new System.EventHandler(this.recDirRefButton_Click);
            // 
            // tvtestRefButton
            // 
            this.tvtestRefButton.Location = new System.Drawing.Point(409, 32);
            this.tvtestRefButton.Margin = new System.Windows.Forms.Padding(2);
            this.tvtestRefButton.Name = "tvtestRefButton";
            this.tvtestRefButton.Size = new System.Drawing.Size(91, 24);
            this.tvtestRefButton.TabIndex = 2;
            this.tvtestRefButton.Text = "参照...";
            this.tvtestRefButton.UseVisualStyleBackColor = true;
            this.tvtestRefButton.Click += new System.EventHandler(this.tvtestRefButton_Click);
            // 
            // recDirBox
            // 
            this.recDirBox.Location = new System.Drawing.Point(117, 67);
            this.recDirBox.Margin = new System.Windows.Forms.Padding(2);
            this.recDirBox.Name = "recDirBox";
            this.recDirBox.Size = new System.Drawing.Size(288, 23);
            this.recDirBox.TabIndex = 4;
            // 
            // tvtestBox
            // 
            this.tvtestBox.Location = new System.Drawing.Point(117, 32);
            this.tvtestBox.Margin = new System.Windows.Forms.Padding(2);
            this.tvtestBox.Name = "tvtestBox";
            this.tvtestBox.Size = new System.Drawing.Size(288, 23);
            this.tvtestBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "TVTestの場所";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "録画保存場所";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.baseicTabPage);
            this.tabControl1.Controls.Add(this.tunerTabPage);
            this.tabControl1.Controls.Add(this.otherTabPage);
            this.tabControl1.Controls.Add(this.other2TabPage);
            this.tabControl1.Controls.Add(this.chatTabPage);
            this.tabControl1.Controls.Add(this.tweetTabPage);
            this.tabControl1.Controls.Add(this.libraryTabPage);
            this.tabControl1.Location = new System.Drawing.Point(10, 10);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(529, 378);
            this.tabControl1.TabIndex = 0;
            // 
            // other2TabPage
            // 
            this.other2TabPage.Controls.Add(this.thumnailPanel);
            this.other2TabPage.Controls.Add(this.thumbnailEnableCheck);
            this.other2TabPage.Location = new System.Drawing.Point(4, 24);
            this.other2TabPage.Name = "other2TabPage";
            this.other2TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.other2TabPage.Size = new System.Drawing.Size(521, 350);
            this.other2TabPage.TabIndex = 7;
            this.other2TabPage.Text = "その他２";
            this.other2TabPage.UseVisualStyleBackColor = true;
            // 
            // thumnailPanel
            // 
            this.thumnailPanel.Controls.Add(this.thumbnailStartMarginBox);
            this.thumnailPanel.Controls.Add(this.thumbnailDelayBox);
            this.thumnailPanel.Controls.Add(this.thumbnailModeComboBox);
            this.thumnailPanel.Controls.Add(this.label42);
            this.thumnailPanel.Controls.Add(this.label43);
            this.thumnailPanel.Controls.Add(this.label44);
            this.thumnailPanel.Location = new System.Drawing.Point(5, 33);
            this.thumnailPanel.Name = "thumnailPanel";
            this.thumnailPanel.Size = new System.Drawing.Size(510, 102);
            this.thumnailPanel.TabIndex = 7;
            // 
            // thumbnailStartMarginBox
            // 
            this.thumbnailStartMarginBox.Location = new System.Drawing.Point(129, 71);
            this.thumbnailStartMarginBox.Name = "thumbnailStartMarginBox";
            this.thumbnailStartMarginBox.Size = new System.Drawing.Size(50, 23);
            this.thumbnailStartMarginBox.TabIndex = 8;
            this.toolTips.SetToolTip(this.thumbnailStartMarginBox, "1x1の画像を取り出す先頭からの時間(秒)");
            // 
            // thumbnailDelayBox
            // 
            this.thumbnailDelayBox.Location = new System.Drawing.Point(129, 42);
            this.thumbnailDelayBox.Name = "thumbnailDelayBox";
            this.thumbnailDelayBox.Size = new System.Drawing.Size(50, 23);
            this.thumbnailDelayBox.TabIndex = 7;
            this.toolTips.SetToolTip(this.thumbnailDelayBox, "録画終了からサムネイル生成を開始するまでの時間");
            // 
            // thumbnailModeComboBox
            // 
            this.thumbnailModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.thumbnailModeComboBox.FormattingEnabled = true;
            this.thumbnailModeComboBox.Items.AddRange(new object[] {
            "1x1と3x4",
            "1x1のみ",
            "3x4のみ"});
            this.thumbnailModeComboBox.Location = new System.Drawing.Point(129, 13);
            this.thumbnailModeComboBox.Name = "thumbnailModeComboBox";
            this.thumbnailModeComboBox.Size = new System.Drawing.Size(91, 23);
            this.thumbnailModeComboBox.TabIndex = 6;
            this.toolTips.SetToolTip(this.thumbnailModeComboBox, "サムネイル生成のモード");
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(89, 16);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(34, 15);
            this.label42.TabIndex = 3;
            this.label42.Text = "モード";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(5, 45);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(118, 15);
            this.label43.TabIndex = 4;
            this.label43.Text = "生成開始ウエイト(ms)";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(21, 74);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(102, 15);
            this.label44.TabIndex = 5;
            this.label44.Text = "スタートマージン(秒)";
            // 
            // thumbnailEnableCheck
            // 
            this.thumbnailEnableCheck.AutoSize = true;
            this.thumbnailEnableCheck.Location = new System.Drawing.Point(21, 12);
            this.thumbnailEnableCheck.Name = "thumbnailEnableCheck";
            this.thumbnailEnableCheck.Size = new System.Drawing.Size(160, 19);
            this.thumbnailEnableCheck.TabIndex = 6;
            this.thumbnailEnableCheck.Text = "サムネイル生成を有効にする";
            this.thumbnailEnableCheck.UseVisualStyleBackColor = true;
            this.thumbnailEnableCheck.CheckedChanged += new System.EventHandler(this.thumbnailEnableCheck_CheckedChanged);
            // 
            // chatTabPage
            // 
            this.chatTabPage.Controls.Add(this.label34);
            this.chatTabPage.Controls.Add(this.niconicoPasswordBox);
            this.chatTabPage.Controls.Add(this.niconicoMailBox);
            this.chatTabPage.Controls.Add(this.label32);
            this.chatTabPage.Controls.Add(this.label33);
            this.chatTabPage.Location = new System.Drawing.Point(4, 24);
            this.chatTabPage.Name = "chatTabPage";
            this.chatTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.chatTabPage.Size = new System.Drawing.Size(521, 350);
            this.chatTabPage.TabIndex = 5;
            this.chatTabPage.Text = "実況コメント";
            this.chatTabPage.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(80, 115);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(396, 30);
            this.label34.TabIndex = 15;
            this.label34.Text = "録画の再生時に実況コメントを表示するには、ニコニコ動画のアカウントが必要です。\r\nメールアドレスとパスワードを指定してください。\r\n";
            // 
            // niconicoPasswordBox
            // 
            this.niconicoPasswordBox.Location = new System.Drawing.Point(117, 67);
            this.niconicoPasswordBox.Name = "niconicoPasswordBox";
            this.niconicoPasswordBox.PasswordChar = '*';
            this.niconicoPasswordBox.Size = new System.Drawing.Size(359, 23);
            this.niconicoPasswordBox.TabIndex = 8;
            // 
            // niconicoMailBox
            // 
            this.niconicoMailBox.Location = new System.Drawing.Point(117, 32);
            this.niconicoMailBox.Name = "niconicoMailBox";
            this.niconicoMailBox.Size = new System.Drawing.Size(359, 23);
            this.niconicoMailBox.TabIndex = 6;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(42, 35);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(70, 15);
            this.label32.TabIndex = 16;
            this.label32.Text = "メールアドレス";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(59, 70);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 15);
            this.label33.TabIndex = 11;
            this.label33.Text = "パスワード";
            // 
            // tweetTabPage
            // 
            this.tweetTabPage.Controls.Add(this.gCalenerPanel);
            this.tweetTabPage.Controls.Add(this.gCalenderEnableCheck);
            this.tweetTabPage.Controls.Add(this.twitterPanel);
            this.tweetTabPage.Controls.Add(this.twitterEnableCheck);
            this.tweetTabPage.Location = new System.Drawing.Point(4, 24);
            this.tweetTabPage.Name = "tweetTabPage";
            this.tweetTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tweetTabPage.Size = new System.Drawing.Size(521, 350);
            this.tweetTabPage.TabIndex = 4;
            this.tweetTabPage.Text = "外部連携";
            this.tweetTabPage.UseVisualStyleBackColor = true;
            // 
            // gCalenerPanel
            // 
            this.gCalenerPanel.Controls.Add(this.label41);
            this.gCalenerPanel.Controls.Add(this.gCalenderIdBox);
            this.gCalenerPanel.Location = new System.Drawing.Point(6, 288);
            this.gCalenerPanel.Name = "gCalenerPanel";
            this.gCalenerPanel.Size = new System.Drawing.Size(509, 52);
            this.gCalenerPanel.TabIndex = 20;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(3, 18);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(83, 15);
            this.label41.TabIndex = 19;
            this.label41.Text = "Calendaer ID";
            // 
            // gCalenderIdBox
            // 
            this.gCalenderIdBox.Location = new System.Drawing.Point(92, 15);
            this.gCalenderIdBox.Name = "gCalenderIdBox";
            this.gCalenderIdBox.Size = new System.Drawing.Size(404, 23);
            this.gCalenderIdBox.TabIndex = 18;
            this.toolTips.SetToolTip(this.gCalenderIdBox, "連携するGoogleカレンダーID");
            // 
            // gCalenderEnableCheck
            // 
            this.gCalenderEnableCheck.AutoSize = true;
            this.gCalenderEnableCheck.Location = new System.Drawing.Point(21, 264);
            this.gCalenderEnableCheck.Name = "gCalenderEnableCheck";
            this.gCalenderEnableCheck.Size = new System.Drawing.Size(164, 19);
            this.gCalenderEnableCheck.TabIndex = 17;
            this.gCalenderEnableCheck.Text = "Googleカレンダーを使用する";
            this.gCalenderEnableCheck.UseVisualStyleBackColor = true;
            this.gCalenderEnableCheck.CheckedChanged += new System.EventHandler(this.gCalenderEnableCheck_CheckedChanged);
            // 
            // twitterPanel
            // 
            this.twitterPanel.Controls.Add(this.tweetTestButton);
            this.twitterPanel.Controls.Add(this.label37);
            this.twitterPanel.Controls.Add(this.twitterapi_secretBox);
            this.twitterPanel.Controls.Add(this.label36);
            this.twitterPanel.Controls.Add(this.twitterapi_keyBox);
            this.twitterPanel.Controls.Add(this.label35);
            this.twitterPanel.Controls.Add(this.twitterStartEnableCheck);
            this.twitterPanel.Controls.Add(this.twitterMessageStartBox);
            this.twitterPanel.Controls.Add(this.label31);
            this.twitterPanel.Controls.Add(this.twitterEndEnableCheck);
            this.twitterPanel.Controls.Add(this.twitterSleepEnableCheck);
            this.twitterPanel.Controls.Add(this.twitterMessageEndBox);
            this.twitterPanel.Controls.Add(this.label30);
            this.twitterPanel.Controls.Add(this.twitterAcceptButton);
            this.twitterPanel.Controls.Add(this.label10);
            this.twitterPanel.Controls.Add(this.twitterUserBox);
            this.twitterPanel.Enabled = false;
            this.twitterPanel.Location = new System.Drawing.Point(6, 36);
            this.twitterPanel.Name = "twitterPanel";
            this.twitterPanel.Size = new System.Drawing.Size(509, 217);
            this.twitterPanel.TabIndex = 16;
            // 
            // tweetTestButton
            // 
            this.tweetTestButton.Location = new System.Drawing.Point(424, 65);
            this.tweetTestButton.Name = "tweetTestButton";
            this.tweetTestButton.Size = new System.Drawing.Size(60, 24);
            this.tweetTestButton.TabIndex = 22;
            this.tweetTestButton.Text = "Tweet !";
            this.tweetTestButton.UseVisualStyleBackColor = true;
            this.tweetTestButton.Click += new System.EventHandler(this.tweetTestButton_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(266, 69);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(143, 15);
            this.label37.TabIndex = 21;
            this.label37.Text = "…認証すると自動で入ります";
            // 
            // twitterapi_secretBox
            // 
            this.twitterapi_secretBox.Location = new System.Drawing.Point(89, 38);
            this.twitterapi_secretBox.Margin = new System.Windows.Forms.Padding(2);
            this.twitterapi_secretBox.Name = "twitterapi_secretBox";
            this.twitterapi_secretBox.Size = new System.Drawing.Size(407, 23);
            this.twitterapi_secretBox.TabIndex = 3;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(18, 41);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(67, 15);
            this.label36.TabIndex = 2;
            this.label36.Text = "api_secret";
            // 
            // twitterapi_keyBox
            // 
            this.twitterapi_keyBox.Location = new System.Drawing.Point(89, 11);
            this.twitterapi_keyBox.Margin = new System.Windows.Forms.Padding(2);
            this.twitterapi_keyBox.Name = "twitterapi_keyBox";
            this.twitterapi_keyBox.Size = new System.Drawing.Size(278, 23);
            this.twitterapi_keyBox.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(33, 14);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(52, 15);
            this.label35.TabIndex = 0;
            this.label35.Text = "api_key";
            // 
            // twitterStartEnableCheck
            // 
            this.twitterStartEnableCheck.AutoSize = true;
            this.twitterStartEnableCheck.Location = new System.Drawing.Point(45, 99);
            this.twitterStartEnableCheck.Margin = new System.Windows.Forms.Padding(2);
            this.twitterStartEnableCheck.Name = "twitterStartEnableCheck";
            this.twitterStartEnableCheck.Size = new System.Drawing.Size(150, 19);
            this.twitterStartEnableCheck.TabIndex = 20;
            this.twitterStartEnableCheck.Text = "録画開始時にツイートする";
            this.twitterStartEnableCheck.UseVisualStyleBackColor = true;
            // 
            // twitterMessageStartBox
            // 
            this.twitterMessageStartBox.Location = new System.Drawing.Point(89, 118);
            this.twitterMessageStartBox.Margin = new System.Windows.Forms.Padding(2);
            this.twitterMessageStartBox.Name = "twitterMessageStartBox";
            this.twitterMessageStartBox.Size = new System.Drawing.Size(407, 23);
            this.twitterMessageStartBox.TabIndex = 19;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(42, 121);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(43, 15);
            this.label31.TabIndex = 18;
            this.label31.Text = "ツイート";
            // 
            // twitterEndEnableCheck
            // 
            this.twitterEndEnableCheck.AutoSize = true;
            this.twitterEndEnableCheck.Location = new System.Drawing.Point(45, 147);
            this.twitterEndEnableCheck.Margin = new System.Windows.Forms.Padding(2);
            this.twitterEndEnableCheck.Name = "twitterEndEnableCheck";
            this.twitterEndEnableCheck.Size = new System.Drawing.Size(150, 19);
            this.twitterEndEnableCheck.TabIndex = 17;
            this.twitterEndEnableCheck.Text = "録画終了時にツイートする";
            this.twitterEndEnableCheck.UseVisualStyleBackColor = true;
            // 
            // twitterSleepEnableCheck
            // 
            this.twitterSleepEnableCheck.AutoSize = true;
            this.twitterSleepEnableCheck.Location = new System.Drawing.Point(45, 195);
            this.twitterSleepEnableCheck.Margin = new System.Windows.Forms.Padding(2);
            this.twitterSleepEnableCheck.Name = "twitterSleepEnableCheck";
            this.twitterSleepEnableCheck.Size = new System.Drawing.Size(149, 19);
            this.twitterSleepEnableCheck.TabIndex = 16;
            this.twitterSleepEnableCheck.Text = "スリープ前後にツイートする";
            this.twitterSleepEnableCheck.UseVisualStyleBackColor = true;
            // 
            // twitterMessageEndBox
            // 
            this.twitterMessageEndBox.Location = new System.Drawing.Point(89, 166);
            this.twitterMessageEndBox.Margin = new System.Windows.Forms.Padding(2);
            this.twitterMessageEndBox.Name = "twitterMessageEndBox";
            this.twitterMessageEndBox.Size = new System.Drawing.Size(407, 23);
            this.twitterMessageEndBox.TabIndex = 13;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(42, 169);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(43, 15);
            this.label30.TabIndex = 12;
            this.label30.Text = "ツイート";
            // 
            // twitterAcceptButton
            // 
            this.twitterAcceptButton.Location = new System.Drawing.Point(393, 10);
            this.twitterAcceptButton.Margin = new System.Windows.Forms.Padding(2);
            this.twitterAcceptButton.Name = "twitterAcceptButton";
            this.twitterAcceptButton.Size = new System.Drawing.Size(91, 24);
            this.twitterAcceptButton.TabIndex = 14;
            this.twitterAcceptButton.Text = "認証...";
            this.twitterAcceptButton.UseVisualStyleBackColor = true;
            this.twitterAcceptButton.Click += new System.EventHandler(this.twitterAcceptButton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 69);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 15);
            this.label10.TabIndex = 10;
            this.label10.Text = "ユーザー名";
            // 
            // twitterUserBox
            // 
            this.twitterUserBox.Location = new System.Drawing.Point(89, 66);
            this.twitterUserBox.Margin = new System.Windows.Forms.Padding(2);
            this.twitterUserBox.Name = "twitterUserBox";
            this.twitterUserBox.ReadOnly = true;
            this.twitterUserBox.Size = new System.Drawing.Size(173, 23);
            this.twitterUserBox.TabIndex = 11;
            // 
            // twitterEnableCheck
            // 
            this.twitterEnableCheck.AutoSize = true;
            this.twitterEnableCheck.Location = new System.Drawing.Point(21, 12);
            this.twitterEnableCheck.Margin = new System.Windows.Forms.Padding(2);
            this.twitterEnableCheck.Name = "twitterEnableCheck";
            this.twitterEnableCheck.Size = new System.Drawing.Size(81, 19);
            this.twitterEnableCheck.TabIndex = 15;
            this.twitterEnableCheck.Text = "ツイートする";
            this.twitterEnableCheck.UseVisualStyleBackColor = true;
            this.twitterEnableCheck.CheckedChanged += new System.EventHandler(this.twitterEnableCheck_CheckedChanged);
            // 
            // libraryTabPage
            // 
            this.libraryTabPage.Controls.Add(this.label45);
            this.libraryTabPage.Controls.Add(this.label40);
            this.libraryTabPage.Controls.Add(this.libAddButton);
            this.libraryTabPage.Controls.Add(this.libDelButton);
            this.libraryTabPage.Controls.Add(this.label39);
            this.libraryTabPage.Controls.Add(this.label38);
            this.libraryTabPage.Controls.Add(this.libDownButton);
            this.libraryTabPage.Controls.Add(this.libUpButton);
            this.libraryTabPage.Controls.Add(this.libView);
            this.libraryTabPage.Controls.Add(this.libButton1);
            this.libraryTabPage.Controls.Add(this.libBox);
            this.libraryTabPage.Location = new System.Drawing.Point(4, 24);
            this.libraryTabPage.Name = "libraryTabPage";
            this.libraryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.libraryTabPage.Size = new System.Drawing.Size(521, 350);
            this.libraryTabPage.TabIndex = 6;
            this.libraryTabPage.Text = "ライブラリ";
            this.libraryTabPage.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.OrangeRed;
            this.label40.Location = new System.Drawing.Point(83, 97);
            this.label40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(382, 15);
            this.label40.TabIndex = 24;
            this.label40.Text = "複数の管理パスを設定した場合，登録動作など，上から順番に処理されます。";
            // 
            // libAddButton
            // 
            this.libAddButton.Location = new System.Drawing.Point(408, 70);
            this.libAddButton.Name = "libAddButton";
            this.libAddButton.Size = new System.Drawing.Size(91, 23);
            this.libAddButton.TabIndex = 8;
            this.libAddButton.Text = "追加";
            this.libAddButton.UseVisualStyleBackColor = true;
            this.libAddButton.Click += new System.EventHandler(this.libAddButton_Click);
            // 
            // libDelButton
            // 
            this.libDelButton.Location = new System.Drawing.Point(460, 211);
            this.libDelButton.Name = "libDelButton";
            this.libDelButton.Size = new System.Drawing.Size(39, 23);
            this.libDelButton.TabIndex = 7;
            this.libDelButton.Text = "削除";
            this.libDelButton.UseVisualStyleBackColor = true;
            this.libDelButton.Click += new System.EventHandler(this.libDelButton_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(28, 97);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(50, 15);
            this.label39.TabIndex = 6;
            this.label39.Text = "管理パス";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(28, 22);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(50, 15);
            this.label38.TabIndex = 5;
            this.label38.Text = "新規パス";
            // 
            // libDownButton
            // 
            this.libDownButton.Location = new System.Drawing.Point(460, 160);
            this.libDownButton.Name = "libDownButton";
            this.libDownButton.Size = new System.Drawing.Size(39, 23);
            this.libDownButton.TabIndex = 4;
            this.libDownButton.Text = "↓";
            this.libDownButton.UseVisualStyleBackColor = true;
            this.libDownButton.Click += new System.EventHandler(this.libDownButton_Click);
            // 
            // libUpButton
            // 
            this.libUpButton.Location = new System.Drawing.Point(460, 131);
            this.libUpButton.Name = "libUpButton";
            this.libUpButton.Size = new System.Drawing.Size(39, 23);
            this.libUpButton.TabIndex = 3;
            this.libUpButton.Text = "↑";
            this.libUpButton.UseVisualStyleBackColor = true;
            this.libUpButton.Click += new System.EventHandler(this.libUpButton_Click);
            // 
            // libView
            // 
            this.libView.FullRowSelect = true;
            this.libView.HideSelection = false;
            this.libView.Indent = 5;
            this.libView.Location = new System.Drawing.Point(31, 115);
            this.libView.Margin = new System.Windows.Forms.Padding(2);
            this.libView.Name = "libView";
            this.libView.ShowLines = false;
            this.libView.ShowNodeToolTips = true;
            this.libView.ShowPlusMinus = false;
            this.libView.ShowRootLines = false;
            this.libView.Size = new System.Drawing.Size(420, 189);
            this.libView.TabIndex = 8;
            // 
            // libButton1
            // 
            this.libButton1.Location = new System.Drawing.Point(408, 40);
            this.libButton1.Name = "libButton1";
            this.libButton1.Size = new System.Drawing.Size(91, 24);
            this.libButton1.TabIndex = 1;
            this.libButton1.Text = "参照...";
            this.libButton1.UseVisualStyleBackColor = true;
            this.libButton1.Click += new System.EventHandler(this.libButton1_Click);
            // 
            // libBox
            // 
            this.libBox.Location = new System.Drawing.Point(31, 40);
            this.libBox.Name = "libBox";
            this.libBox.Size = new System.Drawing.Size(371, 23);
            this.libBox.TabIndex = 0;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.OrangeRed;
            this.label45.Location = new System.Drawing.Point(28, 316);
            this.label45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(0, 15);
            this.label45.TabIndex = 25;
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(548, 426);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tvmaid セットアップ";
            this.otherTabPage.ResumeLayout(false);
            this.otherTabPage.PerformLayout();
            this.tunerTabPage.ResumeLayout(false);
            this.tunerTabPage.PerformLayout();
            this.tunerPanel.ResumeLayout(false);
            this.tunerPanel.PerformLayout();
            this.baseicTabPage.ResumeLayout(false);
            this.baseicTabPage.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.other2TabPage.ResumeLayout(false);
            this.other2TabPage.PerformLayout();
            this.thumnailPanel.ResumeLayout(false);
            this.thumnailPanel.PerformLayout();
            this.chatTabPage.ResumeLayout(false);
            this.chatTabPage.PerformLayout();
            this.tweetTabPage.ResumeLayout(false);
            this.tweetTabPage.PerformLayout();
            this.gCalenerPanel.ResumeLayout(false);
            this.gCalenerPanel.PerformLayout();
            this.twitterPanel.ResumeLayout(false);
            this.twitterPanel.PerformLayout();
            this.libraryTabPage.ResumeLayout(false);
            this.libraryTabPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.SaveFileDialog tvtestDialog;
        private System.Windows.Forms.FolderBrowserDialog recDirDialog;
        private System.Windows.Forms.SaveFileDialog driverDialog;
        private System.Windows.Forms.SaveFileDialog postProcessDialog;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TabPage otherTabPage;
        private System.Windows.Forms.TextBox epgHourBox;
        private System.Windows.Forms.TextBox postProcessBox;
        private System.Windows.Forms.TextBox recFileBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox autoSleepCheck;
        private System.Windows.Forms.Button postProcessRefButton;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tunerTabPage;
        private System.Windows.Forms.Panel tunerPanel;
        private System.Windows.Forms.TreeView tunerBox;
        private System.Windows.Forms.TextBox tunerNameBox;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.Button driverRefButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox driverBox;
        private System.Windows.Forms.Button tunerAddButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox tunerUpdateCheck;
        private System.Windows.Forms.TabPage baseicTabPage;
        private System.Windows.Forms.Button unregStartupButton;
        private System.Windows.Forms.Button regStartupButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button recDirRefButton;
        private System.Windows.Forms.Button tvtestRefButton;
        private System.Windows.Forms.TextBox recDirBox;
        private System.Windows.Forms.TextBox tvtestBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button recDirSpareRefButton;
        private System.Windows.Forms.TextBox recDirSpareBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox postProcessArgBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox postProcessForceCheck;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox recMarginEndBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox recMarginStartBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tvtestRecordWaitBox;
        private System.Windows.Forms.TextBox tvtestServiceWaitBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox epgBasicBCheck;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox epgBasicC2Check;
        private System.Windows.Forms.CheckBox epgBasicC1Check;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox autoSleepIntervalBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox recFileRemoveBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage tweetTabPage;
        private System.Windows.Forms.Button twitterAcceptButton;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox twitterMessageEndBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox twitterUserBox;
        private System.Windows.Forms.CheckBox twitterEnableCheck;
        private System.Windows.Forms.Panel twitterPanel;
        private System.Windows.Forms.CheckBox twitterSleepEnableCheck;
        private System.Windows.Forms.CheckBox twitterEndEnableCheck;
        private System.Windows.Forms.CheckBox twitterStartEnableCheck;
        private System.Windows.Forms.TextBox twitterMessageStartBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox twitterapi_secretBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox twitterapi_keyBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TabPage chatTabPage;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox niconicoPasswordBox;
        private System.Windows.Forms.TextBox niconicoMailBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage libraryTabPage;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button libDownButton;
        private System.Windows.Forms.Button libUpButton;
        private System.Windows.Forms.TreeView libView;
        private System.Windows.Forms.Button libButton1;
        private System.Windows.Forms.TextBox libBox;
        private System.Windows.Forms.Button libDelButton;
        private System.Windows.Forms.Button libAddButton;
        private System.Windows.Forms.FolderBrowserDialog libDirDialog;
        private System.Windows.Forms.Button tweetTestButton;
        private System.Windows.Forms.CheckBox recSubFolderCheck;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TabPage other2TabPage;
        private System.Windows.Forms.CheckBox thumbnailEnableCheck;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel thumnailPanel;
        private System.Windows.Forms.ComboBox thumbnailModeComboBox;
        private System.Windows.Forms.TextBox thumbnailStartMarginBox;
        private System.Windows.Forms.TextBox thumbnailDelayBox;
        private System.Windows.Forms.ToolTip toolTips;
        private System.Windows.Forms.Panel gCalenerPanel;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox gCalenderIdBox;
        private System.Windows.Forms.CheckBox gCalenderEnableCheck;
        private System.Windows.Forms.Label label45;
    }
}

