﻿namespace Tvmaid.Gui
{
    partial class Pincode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pincode));
            this.label1 = new System.Windows.Forms.Label();
            this.twitterPincodeBox = new System.Windows.Forms.TextBox();
            this.twitterAcceptButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "PINCODE:";
            // 
            // twitterPincodeBox
            // 
            this.twitterPincodeBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.twitterPincodeBox.Location = new System.Drawing.Point(120, 29);
            this.twitterPincodeBox.Name = "twitterPincodeBox";
            this.twitterPincodeBox.Size = new System.Drawing.Size(121, 23);
            this.twitterPincodeBox.TabIndex = 1;
            // 
            // twitterAcceptButton
            // 
            this.twitterAcceptButton.Location = new System.Drawing.Point(259, 14);
            this.twitterAcceptButton.Margin = new System.Windows.Forms.Padding(2);
            this.twitterAcceptButton.Name = "twitterAcceptButton";
            this.twitterAcceptButton.Size = new System.Drawing.Size(75, 24);
            this.twitterAcceptButton.TabIndex = 15;
            this.twitterAcceptButton.Text = "認証...";
            this.twitterAcceptButton.UseVisualStyleBackColor = true;
            this.twitterAcceptButton.Click += new System.EventHandler(this.twitterAcceptButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(259, 43);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 16;
            this.cancelButton.Text = "キャンセル";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // Pincode
            // 
            this.AcceptButton = this.twitterAcceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(364, 83);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.twitterAcceptButton);
            this.Controls.Add(this.twitterPincodeBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Pincode";
            this.Text = "Twitter認証";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox twitterPincodeBox;
        private System.Windows.Forms.Button twitterAcceptButton;
        private System.Windows.Forms.Button cancelButton;
    }
}