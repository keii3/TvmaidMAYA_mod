﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Tvmaid
{
    public partial class SetupForm : Form
    {
        MainDefine mainDefine;
        PairList tunerDefine;

        public SetupForm()
        {
            InitializeComponent();

            LoadMainDef();
            LoadTunerDef();
            LoadLibDef();
        }

        void LoadMainDef()
        {
            mainDefine = new MainDefine();
            mainDefine.Load();

            tvtestBox.Text = mainDefine.Data["tvtest"];
            recDirBox.Text = mainDefine.Data["record.folder"];
            recSubFolderCheck.Checked = mainDefine.Data["record.subfolder"] == "on";

            recMarginStartBox.Text = mainDefine.Data["record.margin.start"];
            recMarginEndBox.Text = mainDefine.Data["record.margin.end"];
            //recDirSpareBox.Text = mainDefine.Data["record.folder.spare"];
            recFileBox.Text = mainDefine.Data["record.file"];
            recFileRemoveBox.Text = mainDefine.Data["record.file.remove"];
            epgHourBox.Text = mainDefine.Data["epg.hour"];
            autoSleepCheck.Checked = mainDefine.Data["autosleep"] == "on";
            postProcessBox.Text = mainDefine.Data["postprocess"];
            postProcessArgBox.Text = mainDefine.Data["postprocess.argument"];
            postProcessForceCheck.Checked = mainDefine.Data["postprocess.force"] == "on";

            niconicoMailBox.Text = mainDefine.Data["chat.niconico.mail"];
            niconicoPasswordBox.Text = mainDefine.Data["chat.niconico.password"];

            tvtestServiceWaitBox.Text = mainDefine.Data["tvtest.service.wait"];
            tvtestRecordWaitBox.Text = mainDefine.Data["tvtest.record.wait"];
            epgBasicBCheck.Checked = !mainDefine.Data["epg.basic"].Contains("4"); // BS
            epgBasicC1Check.Checked = !mainDefine.Data["epg.basic"].Contains("6"); // CS1
            epgBasicC2Check.Checked = !mainDefine.Data["epg.basic"].Contains("7"); // CS2

            autoSleepIntervalBox.Text = mainDefine.Data["autosleep.interval"];

            twitterEnableCheck.Checked = mainDefine.Data["tweet.enable"] == "on";
            twitterapi_keyBox.Text = mainDefine.Data["tweet.api_key"];
            twitterapi_secretBox.Text = mainDefine.Data["tweet.api_secret"];
            twitterUserBox.Text = mainDefine.Data["tweet.username"];
            //AppDefine.Main.Data["tweet.userid"] = mainDefine.Data["tweet.userid"];
            twitterStartEnableCheck.Checked = mainDefine.Data["tweet.start.enable"] == "on";
            twitterMessageStartBox.Text = mainDefine.Data["tweet.message.start"];
            twitterEndEnableCheck.Checked = mainDefine.Data["tweet.end.enable"] == "on";
            twitterMessageEndBox.Text = mainDefine.Data["tweet.message"]; // for twet.end

            //AppDefine.Main.Data["tweet.accesstoken"] = mainDefine.Data["tweet.accesstoken"];
            //AppDefine.Main.Data["tweet.accesstokensecret"] = mainDefine.Data["tweet.accesstokensecret"];
            twitterSleepEnableCheck.Checked = mainDefine.Data["tweet.sleep.enable"] == "on";

            gCalenderEnableCheck.Checked = mainDefine.Data["google.calendar"] == "on";
            gCalenderIdBox.Text = mainDefine.Data["google.calendarid"];

            thumbnailEnableCheck.Checked = mainDefine.Data["thumbnail.enable"] == "on";
            thumbnailModeComboBox.SelectedIndex = mainDefine.Data["thumbnail.mode"].ToInt();
            thumbnailDelayBox.Text = mainDefine.Data["thumbnail.delay"];
            thumbnailStartMarginBox.Text = mainDefine.Data["thumbnail.start"];

        }

        void LoadTunerDef()
        {
            tunerDefine = new PairList(Util.GetUserPath("tuner.def"));
            tunerDefine.Load();

            foreach (var pair in tunerDefine)
                tunerBox.Nodes.Add(pair.Key + "=" + pair.Value);
        }

        void LoadLibDef()
        {
            string text;
            using (var sr = new StreamReader(Util.GetUserPath("library.def")))
                text = sr.ReadToEnd();

            foreach (var line in text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
                libView.Nodes.Add(line);

        }

        //設定完了
        private void endButton_Click(object sender, EventArgs arg)
        {
            try
            {
                SaveMainDefine();

                if (tunerUpdateCheck.Checked)
                {
                    SaveTunerDef();
                    Program.IsTunerUpdate = true;
                }

                SaveLibraryDef();

                var res = MessageBox.Show("Tvmaidを再起動して、設定を反映しますか？", Program.Name, MessageBoxButtons.OKCancel);

                if (res == DialogResult.OK)
                {
                    this.DialogResult = DialogResult.Yes;
                    Program.IsReboot = true;
                }

                Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Program.Name);
            }
        }

        private void SaveMainDefine()
        {
            mainDefine.Data["tvtest"] = tvtestBox.Text;
            mainDefine.Data["record.folder"] = recDirBox.Text;
            mainDefine.Data["record.subfolder"] = recSubFolderCheck.Checked ? "on" : "off";

            mainDefine.Data["record.margin.start"] = recMarginStartBox.Text;
            mainDefine.Data["record.margin.end"] = recMarginEndBox.Text;
            //mainDefine.Data["record.folder.spare"] = recDirSpareBox.Text;
            mainDefine.Data["record.file"] = recFileBox.Text;
            mainDefine.Data["record.file.remove"] = recFileRemoveBox.Text;

            mainDefine.Data["epg.hour"] = epgHourBox.Text;
            mainDefine.Data["autosleep"] = autoSleepCheck.Checked ? "on" : "off";
            mainDefine.Data["postprocess"] = postProcessBox.Text;
            
            mainDefine.Data["chat.niconico.mail"] = niconicoMailBox.Text;
            mainDefine.Data["chat.niconico.password"] = niconicoPasswordBox.Text;

            mainDefine.Data["postprocess.argument"] = postProcessArgBox.Text;
            mainDefine.Data["postprocess.force"] = postProcessForceCheck.Checked ? "on" : "off";
            mainDefine.Data["tvtest.service.wait"] = tvtestServiceWaitBox.Text;
            mainDefine.Data["tvtest.record.wait"] = tvtestRecordWaitBox.Text;

            var list = new List<string>();
            if (!epgBasicBCheck.Checked) list.Add("4"); // BS
            if (!epgBasicC1Check.Checked) list.Add("6"); // CS1
            if (!epgBasicC2Check.Checked) list.Add("7"); //CS2
            mainDefine.Data["epg.basic"] = string.Join(",", list);

            mainDefine.Data["autosleep.interval"] = autoSleepIntervalBox.Text;

            mainDefine.Data["tweet.enable"] = twitterEnableCheck.Checked ? "on" : "off";
            mainDefine.Data["tweet.api_key"] = twitterapi_keyBox.Text;
            mainDefine.Data["tweet.api_secret"] = twitterapi_secretBox.Text;
            mainDefine.Data["tweet.accesstoken"] = AppDefine.Main.Data["tweet.accesstoken"];
            mainDefine.Data["tweet.accesstokensecret"] = AppDefine.Main.Data["tweet.accesstokensecret"];
            mainDefine.Data["tweet.username"] = twitterUserBox.Text;
            mainDefine.Data["tweet.userid"] = AppDefine.Main.Data["tweet.userid"];
            mainDefine.Data["tweet.start.enable"] = twitterStartEnableCheck.Checked ? "on" : "off";
            mainDefine.Data["tweet.message.start"] = twitterMessageStartBox.Text;
            mainDefine.Data["tweet.end.enable"] = twitterEndEnableCheck.Checked ? "on" : "off";
            mainDefine.Data["tweet.message"] = twitterMessageEndBox.Text;
            mainDefine.Data["tweet.sleep.enable"] = twitterSleepEnableCheck.Checked ? "on" : "off";

            mainDefine.Data["google.calendar"] = gCalenderEnableCheck.Checked ? "on" : "off";
            mainDefine.Data["google.calendarid"] = gCalenderIdBox.Text;

            mainDefine.Data["thumbnail.enable"] = thumbnailEnableCheck.Checked ? "on" : "off";
            mainDefine.Data["thumbnail.mode"]   = thumbnailModeComboBox.SelectedIndex.ToString();
            mainDefine.Data["thumbnail.delay"]  = thumbnailDelayBox.Text;
            mainDefine.Data["thumbnail.start"]  = thumbnailStartMarginBox.Text;

            mainDefine.Data.Save();
        }

        private void SaveTunerDef()
        {
            tunerDefine.Clear();

            foreach (TreeNode item in tunerBox.Nodes)
            {
                var data = item.Text.Split(new char[] { '=' });
                tunerDefine[data[0]] = data[1];
            }

            tunerDefine.Save();
        }

        private void SaveLibraryDef()
        {
            using (var sw = new StreamWriter(Util.GetUserPath("library.def"), false, Encoding.GetEncoding("utf-8")))
            {
                foreach (TreeNode line in libView.Nodes)
                    sw.WriteLine(line.Text);
            }
        }

        //参照
        private void tvtestRefButton_Click(object sender, EventArgs e)
        {
            var res = this.tvtestDialog.ShowDialog();

            if (res == DialogResult.OK)
                tvtestBox.Text = tvtestDialog.FileName;
        }

        //参照
        private void recDirRefButton_Click(object sender, EventArgs e)
        {
            var res = recDirDialog.ShowDialog();

            if (res == DialogResult.OK)
                recDirBox.Text = recDirDialog.SelectedPath;
        }

        //参照
        private void recDirSpareRefButton_Click(object sender, EventArgs e)
        {
            var res = recDirDialog.ShowDialog();

            if (res == DialogResult.OK)
                recDirSpareBox.Text = recDirDialog.SelectedPath;
        }

        //参照
        private void driverRefButton_Click(object sender, EventArgs e)
        {
            if (File.Exists(tvtestBox.Text))
                driverDialog.InitialDirectory = Path.GetDirectoryName(tvtestBox.Text);

            var res = driverDialog.ShowDialog();
            if (res == DialogResult.OK)
                driverBox.Text = driverDialog.FileName;
        }

        //参照
        private void postProcessRefButton_Click(object sender, EventArgs e)
        {
            var res = this.postProcessDialog.ShowDialog();

            if (res == DialogResult.OK)
                postProcessBox.Text = postProcessDialog.FileName;
        }

        //追加
        private void tunerAddButton_Click(object sender, EventArgs arg)
        {
            try
            {
                if (tunerNameBox.Text == "")
                    throw new Exception("チューナ名を入力してください。");
                if (tunerNameBox.Text.IndexOf('=') != -1)
                    throw new Exception("チューナ名に「=」は使えません。");
                if (File.Exists(driverBox.Text) == false)
                    throw new Exception("Bonドライバのパスが間違っています。");

                foreach (TreeNode item in tunerBox.Nodes)
                {
                    var tuner = item.Text.Split(new char[] { '=' });

                    if (tunerNameBox.Text == tuner[0])
                        throw new Exception("同じ名前のチューナを複数指定できません。");
                }

                tunerBox.Nodes.Add(tunerNameBox.Text + "=" + driverBox.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Program.Name);
            }
        }

        //上へ
        private void upButton_Click(object sender, EventArgs e)
        {
            if (tunerBox.SelectedNode != null)
                ReplaceNode(tunerBox.SelectedNode, tunerBox.SelectedNode.PrevNode);
        }

        //下へ
        private void downButton_Click(object sender, EventArgs e)
        {
            if (tunerBox.SelectedNode != null)
                ReplaceNode(tunerBox.SelectedNode, tunerBox.SelectedNode.NextNode);
        }

        void ReplaceNode(TreeNode node1, TreeNode node2)
        {
            if (node1 != null && node2 != null)
            {
                var text = node1.Text;
                node1.Text = node2.Text;
                node2.Text = text;
                tunerBox.SelectedNode = node2;
            }
        }

        //削除
        private void removeButton_Click(object sender, EventArgs e)
        {
            if (tunerBox.SelectedNode != null)
            {
                tunerBox.Nodes.Remove(tunerBox.SelectedNode);
            }            
        }
               
        private void tunerUpdateCheck_CheckedChanged(object sender, EventArgs e)
        {
            tunerPanel.Enabled = tunerUpdateCheck.Checked;
        }

        private void regStartupButton_Click(object sender, EventArgs e)
        {
            var msg =
                "スタートアップに登録していいですか？\n"
                + "注意！ レジストリのスタートアップに登録します。\n"
                + "この機能は使用せず、自分でショーカットをスタートメニューに置いてもかまいません。";

            if (MessageBox.Show(msg, Program.Name, MessageBoxButtons.OKCancel) != DialogResult.OK) return;

            var key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true);
            key.SetValue("TvmaidMAYA", Util.GetBasePath("Tvmaid.exe"));
            key.Close();

            MessageBox.Show("登録しました。", Program.Name);
        }

        private void unregStartupButton_Click(object sender, EventArgs e)
        {
            var msg = "レジストリのスタートアップ設定を削除していいですか？";

            if (MessageBox.Show(msg, Program.Name, MessageBoxButtons.OKCancel) != DialogResult.OK) return;

            var key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true);
            key.DeleteValue("TvmaidMAYA", false);
            key.Close();

            MessageBox.Show("削除しました。", Program.Name);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void postProcessBoxExt_TextChanged(object sender, EventArgs e) { }

        private void twitterEnableCheck_CheckedChanged(object sender, EventArgs e)
        {
            twitterPanel.Enabled = twitterEnableCheck.Checked;
        }

        private void twitterAcceptButton_Click(object sender, EventArgs e)
        {
            string key = twitterapi_keyBox.Text;
            string secret = twitterapi_secretBox.Text;

            if (key.Length == 0 || secret.Length == 0)
            {
                var msg = "Tweet機能を使用する場合，予めapi_key/api_secretを取得する必要が有ります。\n";
                MessageBox.Show(msg, Program.Name, MessageBoxButtons.OK);
                return;
            }
            Twitter.Certification(key, secret);
            twitterUserBox.Text = AppDefine.Main.Data["tweet.username"];
        }

        //参照
        private void libButton1_Click(object sender, EventArgs e)
        {

            var res = libDirDialog.ShowDialog();

            if (res == DialogResult.OK)
                libBox.Text = libDirDialog.SelectedPath;
        }

        private void libDelButton_Click(object sender, EventArgs e)
        {
            if (libView.SelectedNode != null)
            {
                libView.Nodes.Remove(libView.SelectedNode);
            }

        }

        private void libUpButton_Click(object sender, EventArgs e)
        {
            if (libView.SelectedNode != null)
                ReplaceNodeLib(libView.SelectedNode, libView.SelectedNode.PrevNode);

        }

        private void libDownButton_Click(object sender, EventArgs e)
        {
            if (libView.SelectedNode != null)
                ReplaceNodeLib(libView.SelectedNode, libView.SelectedNode.NextNode);

        }
        void ReplaceNodeLib(TreeNode node1, TreeNode node2)
        {
            if (node1 != null && node2 != null)
            {
                var text = node1.Text;
                node1.Text = node2.Text;
                node2.Text = text;
                libView.SelectedNode = node2;
            }
        }

        private void libAddButton_Click(object sender, EventArgs arg)
        {
            try
            {
                if (libBox.Text == "")
                    throw new Exception("管理対象のパスを入力してください。");

                foreach (TreeNode item in libView.Nodes)
                {
                    var path = item.Text;

                    if (libBox.Text == path)
                        throw new Exception("登録済みのパスです。");
                }

                libView.Nodes.Add(libBox.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Program.Name);
            }
        }

        private void tweetTestButton_Click(object sender, EventArgs arg)
        {
            // Tweet Test
            try
            {
                Twitter.Send(Environment.MachineName + "からTweetのテストです\r\n"
                    + "今の時間は" + DateTime.Now.ToString("HH: mm") + "です。"
                );

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Program.Name);
            }

        }

        private void gCalenderEnableCheck_CheckedChanged(object sender, EventArgs e)
        {
            thumnailPanel.Enabled = thumbnailEnableCheck.Checked;
        }

        private void thumbnailEnableCheck_CheckedChanged(object sender, EventArgs e)
        {
            thumnailPanel.Enabled = thumbnailEnableCheck.Checked;
        }
        
    }
}
