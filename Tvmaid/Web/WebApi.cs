﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Tvmaid.Chat;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading;

using System.Management; // for WMI
using Codeplex.Data;     // DynamicJson
using System.Text;

namespace Tvmaid
{
    //スクリプトから呼ばれるメソッドはpublicにすること
    class WebApi : WebApiBase
    {
        public WebApi(HttpListenerContext con) : base(con) { }

        public void GetTable()
        {
            using (var tvdb = new Tvdb(true))
            {
                tvdb.Sql = GetQuery("sql");
                ret.data1 = tvdb.GetList();
            }
        }

        //予約削除
        public void RemoveReserve()
        {
            var id = GetQuery("id");

            using (var tvdb = new Tvdb(true))
            {
                var res = new Reserve(tvdb, id.ToInt());

                //自動予約(または終わっている番組)の場合は、削除せずに無効にする
                if (res.Auto == -1 || res.EndTime < DateTime.Now)
                    res.Remove(tvdb);
                else
                    res.SetEnable(tvdb, false);
            }
        }

        //予約追加、更新
        //statusは、有効、追従のビット以外は無視される
        public void AddReserve()
        {
            var res = SetReserveQuery(); //送信された値を入力

            using (var tvdb = new Tvdb(true))
            {
                //追従モードなら番組情報を入力
                if ((res.Status & (int)Reserve.StatusCode.EventMode) > 0)
                {
                    var ev = new Event(tvdb, res.Fsid, res.Eid);
                    res.Fsid = ev.Fsid;
                    res.Eid = ev.Eid;
                    res.StartTime = ev.Start;
                    res.Duration = ev.Duration;
                    res.Title = GetQuery("title", ev.Title);
                    res.Smargin = GetQuery("smargin", -1);
                    res.Emargin = GetQuery("emargin", -1);
                }
                res.Add(tvdb);
            }
            ret.data1 = res.Id;
        }

        Reserve SetReserveQuery()
        {
            Reserve res;
            var id = GetQuery("id", -1);

            if (id == -1)
                res = new Reserve();
            else
            {
                using (var tvdb = new Tvdb(true))
                    res = new Reserve(tvdb, id);

                if ((res.Status & (int)Reserve.StatusCode.Recoding) > 0)
                    throw new Exception("録画中のため、予約を変更できません。");
            }

            res.Fsid = GetQuery("fsid", res.Fsid);
            res.Eid = GetQuery("eid", res.Eid);

            res.StartTime = new DateTime(base.GetQuery("start", res.StartTime.Ticks));
            res.Duration = GetQuery("duration", res.Duration);

            res.Title = GetQuery("title", res.Title);
            res.TunerName = GetQuery("tuner", res.TunerName);

            var status = GetQuery("status", -1);
            if (status != -1)
            {
                status &= 3 + 4;            //有効、追従、チューナ固定のビット以外を削除
                res.Status &= 255 - 3 - 4;  //有効、追従、チューナ固定のビット以外を削除
                res.Status |= status;       //合成する
            }

            return res;
        }

        //予約チューナ再割り当て
        public void ResetReserveTuner()
        {
            using (var tvdb = new Tvdb(true))
                Reserve.ResetTuner(tvdb);
        }

        //自動予約削除
        public void RemoveAutoReserve()
        {
            var id = GetQuery("id");

            using (var tvdb = new Tvdb(true))
            {
                var auto = new AutoReserve(tvdb, id.ToInt());
                auto.Remove(tvdb);
            }
        }

        //自動予約追加、更新
        public void AddAutoReserve()
        {
            var auto = SetAutoReserveQuery();

            using (var tvdb = new Tvdb(true))
                auto.Add(tvdb);

            ret.data1 = auto.Id;
        }

        AutoReserve SetAutoReserveQuery()
        {
            AutoReserve auto;
            var id = GetQuery("id", -1);

            if (id == -1)
                auto = new AutoReserve();
            else
            {
                using (var tvdb = new Tvdb(true))
                    auto = new AutoReserve(tvdb, id);
            }

            auto.Name = GetQuery("name", auto.Name);
            auto.Folder = GetQuery("folder", auto.Folder);

            auto.Query = GetQuery("query", auto.Query);
            auto.Option = GetQuery("option", auto.Option);
            auto.Status = GetQuery("status", auto.Status);

            auto.Recstat = GetQuery("recstat", auto.Recstat);
            auto.Smargin = GetQuery("smargin", auto.Smargin);
            auto.Emargin = GetQuery("emargin", auto.Emargin);

            return auto;
        }

        //録画フォルダ空き取得
        public void GetRecordFolderFree()
        {
            var dir = AppDefine.Main.Data["record.folder"];
            ulong free, total, totalFree;

            if (GetDiskFreeSpaceEx(dir, out free, out total, out totalFree))
                ret.data1 = free;
            else
                throw new Exception("録画フォルダの空き容量が取得できませんでした。");
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetDiskFreeSpaceEx(
            string lpDirectoryName,
            out ulong lpFreeBytesAvailable,
            out ulong lpTotalNumberOfBytes,
            out ulong lpTotalNumberOfFreeBytes);

        //ユーザ番組表設定
        public void SetUserEpg()
        {
            var id = GetQuery("id").ToInt();
            var list = GetQuery("fsid").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            using (var tvdb = new Tvdb(true))
            {
                try
                {
                    tvdb.BeginTrans();

                    tvdb.Sql = "delete from user_epg where id = " + id;
                    tvdb.Execute();

                    for (var i = 0; i < list.Length; i++)
                    {
                        var fsid = list[i].ToLong();
                        tvdb.Sql = @"insert into user_epg values({0}, {1}, {2});".Formatex(id, fsid, i);
                        tvdb.Execute();
                    }
                    tvdb.Commit();
                }
                catch
                {
                    tvdb.Rollback();
                    throw;
                }
            }
        }

        public void GetLog()
        {
            using (var ldb = new Ldb(true))
            {
                ldb.Sql = GetQuery("sql");
                ret.data1 = ldb.GetList();
            }
        }

        public void ClearLog()
        {
            using (var ldb = new Ldb(true))
            {
                ldb.Sql = "delete from log";
                ldb.Execute();
            }

            Log.Info("ログをクリアしました。");
        }

        public void ClearRecord()
        {
            using (var tvdb = new Tvdb(true))
            {
                tvdb.Sql = "delete from record";
                tvdb.Execute();
            }
            // keii
            var file = Util.GetUserPath() + "\\thumb\\*.jpg";
            ShellFile.Delete(file);
            //
            Log.Info("録画をクリアしました。");
        }

        public void CancelUpdateEpg()
        {
            RecTimer.CancelUpdateEpg();
        }

        public void UpdateEpg()
        {
            RecTimer.UpdateEpg();
        }

        int GetTunerState(string tunerName, Tvdb tvdb)
        {
            Tuner tuner;

            tuner = new Tuner(tvdb, tunerName);

            var server = new TvServer(tuner);
            var state = server.GetState();

            //番組表更新中は5
            if (state == TvServerApi.State.View)
                return EpgUpdater.Running ? 5 : 0;
            else
                return (int)state;
        }

        public void GetTunersState()
        {
            var list = new List<object[]>();

            using (var tvdb = new Tvdb(true))
            {
                tvdb.Sql = "select name from tuner order by id";

                using (var t = tvdb.GetTable())
                {
                    while (t.Read())
                    {
                        var val = new object[2];
                        val[0] = t.GetStr(0);
                        list.Add(val);
                    }
                }

                foreach (var val in list)
                    val[1] = GetTunerState((string)val[0], tvdb);
            }

            ret.data1 = list;
        }

        public void ShowServer()
        {
            var tunerName = GetQuery("tuner");
            var fsid = GetQuery("fsid").ToLong();
            Tuner tuner;
            Service service;

            using (var tvdb = new Tvdb(true))
            {
                tuner = new Tuner(tvdb, tunerName);
                service = new Service(tvdb, fsid);
            }

            var server = new TvServer(tuner);
            server.Open(true);
            server.SetService(service);
        }

        public void CloseServer()
        {
            var tunerName = GetQuery("tuner");
            Tuner tuner;

            using (var tvdb = new Tvdb(true))
                tuner = new Tuner(tvdb, tunerName);

            var server = new TvServer(tuner);
            server.Close();
        }

        public void RemoveFile()
        {
            var id = GetQuery("id").ToInt();

            Record record;

            using (var tvdb = new Tvdb(true))
                record = new Record(tvdb, id);

            //var file = Path.Combine(AppDefine.Main.Data["record.folder"], record.File);
            //if (File.Exists(file))
            //    ShellFile.Delete(file);

            List<string> recFolder = Util.GetRecPath();

            for (int i = 0; i < recFolder.Count; i++)
            {
                IEnumerable<string> files = FindFile.SearchFile(recFolder[i], record.File, AppDefine.Main.Data["record.subfolder"] == "on" ? true : false);
                                            
                foreach(var file in files)
                {
                    if (File.Exists(file))
                    {
                        ShellFile.Delete(file);
                        Util.RemoveThumbnail(id);

                        i = recFolder.Count;
                        break;
                    }
                }
            }

            UpdateRecordStatus();
        }

        public void UpdateRecordStatus()
        {
            /* original
            var recFolder = AppDefine.Main.Data["record.folder"];
            var files = Directory.GetFiles(recFolder);

            using (var tvdb = new Tvdb(true))
            {
                tvdb.Sql = "update record set status = status | {0}".Formatex((int)Record.StatusCode.Delete);
                tvdb.Execute();

                foreach (var file in files)
                {
                    var name = Path.GetFileName(file);

                    tvdb.Sql = "update record set status = status - {0} where file = '{1}'".Formatex((int)Record.StatusCode.Delete, Tvdb.SqlEncode(name));
                    tvdb.Execute();
                }
            }
            */

            using (var tvdb = new Tvdb(true))
            {
                tvdb.Sql = "update record set status = status | {0}".Formatex((int)Record.StatusCode.Delete); // 一旦，全部 "Delete"付けて削除済みへ
                tvdb.Execute();
            }

            List<string> recFolder = Util.GetRecPath();
            for (int i = 0; i < recFolder.Count; i++)
            {
                FileListArg list = new FileListArg
                {
                    File = recFolder[i],
                    Date = new DateTime()
                };

                Regex regex = new Regex(@"\.ts$|\.mp4$|\.m4v$");
                IEnumerable<FileListArg> files = from file in FindFile.GetAllFilesWithDate(list, AppDefine.Main.Data["record.subfolder"] == "on"? false : true)
                                                   where regex.IsMatch(file.File)
                                                   orderby file.Date descending, file.File
                                                   select file;

                using (var tvdb = new Tvdb(true))
                {
                    foreach (var file in files)
                    {
                        var name = Path.GetFileName(file.File);
                        var dt = file.Date.Ticks;
                        tvdb.Sql = @"update record set status = (status - (status & {0})) where file = '{1}' and start <= {2} and reserve_end >= {2} - 30000000".Formatex(
                                (int)Record.StatusCode.Delete, 
                                Tvdb.SqlEncode(name), 
                                dt.ToString()
                            );
                        tvdb.Execute();
                    }
                }
            }
        }

        public void StartHls()
        {
            var streamId = GetQuery("stream");
            var ready = GetQuery("ready", 1);

            if (GetQuery("type") == "live")
            {
                var tuner = GetQuery("tuner");
                var fsid = GetQuery("fsid").ToLong();
                var mode = GetQuery("mode");

                HlsStream.Start(streamId, tuner, fsid, mode);
            }
            else
            {
                var id = GetQuery("id").ToInt();
                var start = GetQuery("start").ToInt();
                var mode = GetQuery("mode");
                var req = GetQuery("req", "");

                HlsStream.Start(streamId, id, start, mode, req);
            }

            var stream = HlsStream.GetStream(streamId);

            while (stream.GetPlaylistCount < ready)
            {
                if (stream.IsStop)
                    throw new Exception("ストリームの開始に失敗しました。");

                System.Threading.Thread.Sleep(100);
            }
        }

        public void StopHls()
        {
            var stream = GetQuery("stream");
            HlsStream.Stop(stream);
        }

        static bool tsFileInfoRunning = false;

        public void AddTsFileInfo()
        {
            if (tsFileInfoRunning)
                throw new Exception("録画ファイル登録は、すでに実行中です。");

            tsFileInfoRunning = true;
            Log.Info("録画ファイル登録を開始します。");

            Task.Factory.StartNew(() =>
            {
                List<string> recFolder = Util.GetRecPath();
                for (int i = 0; i < recFolder.Count; i++)
                {
                    FileListArg list = new FileListArg
                    {
                        File = recFolder[i],
                        Date = new DateTime()
                    };
                    
                    Regex regex = new Regex(@"\.ts$|\.mp4$|\.m4v$");
                    IEnumerable<FileListArg> files = from file in FindFile.GetAllFilesWithDate(list, AppDefine.Main.Data["record.subfolder"] == "on" ? false : true)
                                                       where regex.IsMatch(file.File) 
                                                       orderby file.Date descending, file.File 
                                                       select file;

                    using (var tvdb = new Tvdb(true))
                    {
                        foreach (var file in files)
                        {
                            string fullpath = file.File;

                            tvdb.Sql = "select id from record where file = '{0}'".Formatex(Tvdb.SqlEncode(Path.GetFileName(fullpath)));
                            using (var t = tvdb.GetTable())
                                if (t.Read())
                                    continue;   //すでに登録済み

                            int dura = 0;
                            var starttime = DateTime.MinValue;
                            try
                            {
                                if (Path.GetExtension(fullpath) == ".ts")
                                {
                                    var info = new TsFileInfo(fullpath);
                                    var rec = new Record
                                    {
                                        Title = info.Title,
                                        ServiceName = info.Service,
                                        Desc = info.Desc,
                                        LongDesc = info.LongDesc,
                                        GenreText = info.Genre,
                                        File = info.File,
                                        Start = info.StartTime,
                                        End = info.EndTime,
                                        ReserveStart = starttime = info.ReserveStart,
                                        ReserveEnd = info.ReserveEnd,
                                        Message = "[TSファイル登録]"
                                    };
                                    rec.Add(tvdb);

                                    dura = (int)(info.EndTime - info.StartTime).TotalSeconds;
                                }
                                else
                                {
                                    var info = new Mp4FileInfo(fullpath);
                                    var rec = new Record
                                    {
                                        Title = info.Title + " " + info.Subtitle,
                                        ServiceName = info.Service,
                                        Desc = info.Desc,
                                        LongDesc = info.LongDesc,
                                        GenreText = info.Genre,
                                        File = info.File,
                                        Start = info.StartTime,
                                        End = info.EndTime,
                                        ReserveStart = starttime = info.StartTime,
                                        ReserveEnd = info.EndTime,
                                        Message = "[MP4ファイル登録]"
                                    };
                                    rec.Add(tvdb);

                                    dura = (int)(info.EndTime - info.StartTime).TotalSeconds;
                                }

                                Log.Info("登録: " + Path.GetFileName(file.File));
                            }
                            catch (Exception ex)
                            {
                                Log.Error("{0} を登録できませんでした。{1}".Formatex(Path.GetFileName(fullpath), ex.Message));
                            }
                        } // foreach files
                    } // using tvdb

                    using (var tvdb = new Tvdb(true))
                        for (var j = 1; j < 3; j++)
                            foreach (var file in files)
                            {
                                string fullpath = file.File;
                                var op = Util.GetUserPath("thumb");

                                tvdb.Sql = @"select id, file, start, end from record where status=0 and file='{0}'".Formatex(Path.GetFileName(fullpath));
                                using (var t = tvdb.GetTable())
                                    if (t.Read())
                                    {
                                        var id = t.GetInt(0);
                                        var starttime = new DateTime(t.GetLong(2));
                                        var endtime = new DateTime(t.GetLong(3));
                                        var dura = (int)(endtime - starttime).TotalSeconds;
                                        int margin = AppDefine.Main.Data["record.margin.start"].ToInt() + AppDefine.Main.Data["thumbnail.start"].ToInt();

                                        FileInfo fi = new FileInfo(op + "\\" + id.ToString() + (j == 1 ? ".jpg" : ".m.jpg"));
                                        if (starttime > fi.LastWriteTime) // DB登録日より古いサムネは作り直し
                                            Util.MakeThumbnail(fullpath, id, margin, dura, j);  // make Thumbneil
                                    }
                            }

                } // foreach recFolder
                Log.Info("録画ファイル登録が完了しました。");

            }, TaskCreationOptions.AttachedToParent)
            .ContinueWith(_ =>
            {
                tsFileInfoRunning = false;
            });
        }

        static Dictionary<string, ChatServer> ChatServers = new Dictionary<string, ChatServer>(); //接続中サーバリスト

        public void OpenChatServer()
        {
            var type = GetQuery("type").ToInt();
            var id = GetQuery("id");
            var fsid = GetQuery("fsid").ToLong();

            lock (ChatServers)
            {
                ChatServer server = null;

                if (ChatServers.ContainsKey(id))
                    ChatServers.Remove(id);

                switch (type)
                {
                    case 1:
                        server = new NiconicoLiveServer();
                        break;
                    case 2:
                        var mail = AppDefine.Main.Data["chat.niconico.mail"];
                        var password = AppDefine.Main.Data["chat.niconico.password"];

                        if (mail == null || mail == "" || password == null || password == "")
                            throw new Exception("メールアドレスまたはパスワードが設定されていないため、ニコニコ実況過去ログは使用できません。");
                        else
                            server = new NiconicoLogServer(mail, password, GetQuery("start").ToInt(), GetQuery("end").ToInt());
                        break;
                    default:
                        throw new Exception("定義されていないChatServerです。");
                }

                server.Open(fsid);
                ChatServers.Add(id, server);
                server.Active.Start();

                RemoveChatServer();
            }
        }

        void RemoveChatServer()
        {
            var list = new List<string>();

            foreach (var server in ChatServers)
                if (server.Value.Active.ElapsedMilliseconds > 60 * 1000)
                    list.Add(server.Key);

            foreach (var key in list)
                ChatServers.Remove(key);
        }

        public void GetChat()
        {
            var id = GetQuery("id");
            var time = GetQuery("time").ToInt();
            var max = GetQuery("max").ToInt();

            lock (ChatServers)
            {
                if (ChatServers.ContainsKey(id))
                    ret.data1 = ChatServers[id].GetChat(time, max);
                else
                    throw new Exception("指定されたIDのChatServerがありません。");

                ChatServers[id].Active.Restart();
            }
        }

        // keii ---
        public void GetPcName() // WebUIに表示するPC名を取得
        {
            ret.SetCode(0, Environment.MachineName);
        }

        public void GetUserEpgName() // ユーザー番組表の表示名を取得
        {
            var list = new List<object[]>();

            for (int i = 1; i <= 3; i++)
            {
                var val = new object[2];
                val[0] = "userepg.user" + i.ToString("D") + ".name";
                val[1] = AppDefine.Main.Data["userepg.user" + i.ToString("D") + ".name"];
                list.Add(val);
            }

            ret.data1 = list;
        }

        public void GoSleep() // WebUIからSleep発行
        {
            Log.Info("WebUI経由で，スリープへ移行します。");

            SleepMan sleepMan = new SleepMan(); // これで１回 SetSleep()が呼ばれる
            //sleepMan.SetSleep();
        }
        
        public void GetLibTable() // GetTable()のLibrary用
        {
            using (var libdb = new Libdb(true))
            {
                libdb.Sql = GetQuery("sql");
                ret.data1 = libdb.GetList();
            }
        }

        static bool libInfoRunning = false;
        static CancellationTokenSource _addlibtokenSource = null;

        public void AddLibInfo() // ライブラリに登録(AddTsFileInfo()のパクリ)
        {
            if (libInfoRunning)
                throw new Exception("Library登録は、すでに実行中です。");

            libInfoRunning = true;
            Log.Info("Library登録を開始します。");

            if (_addlibtokenSource == null) _addlibtokenSource = new CancellationTokenSource();
            var token = _addlibtokenSource.Token;

            Task.Factory.StartNew(() =>
            {
                string text;
                string dir = Util.GetUserPath("thumblib"); // Thumbnail Directory

                // make thumbnail Directory
                if (Directory.Exists(dir) == false)
                    Directory.CreateDirectory(dir);

                using (var libdb = new Libdb(true))
                {
                    lock (libdb)
                    {
                        libdb.Sql = @"update library set is_check=1";
                        libdb.Execute();
                    }
                }

                using (var sr = new StreamReader(Util.GetUserPath("library.def"))) // read Library Directory
                    text = sr.ReadToEnd();

                foreach (var line in text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    token.ThrowIfCancellationRequested(); // Cancel時，ここで停止(1/3)

                    if (Directory.Exists(line) == false)
                        continue;

                    FileListArg list = new FileListArg
                    {
                        File = line,
                        Date = new DateTime()
                    };

                    Regex regex = new Regex(@"\.ts$|\.mp4$|\.m4v$");
                    IEnumerable<FileListArg> files = from file in FindFile.GetAllFilesWithDate(list)
                                                       where regex.IsMatch(file.File)
                                                       orderby file.Date descending, file.File
                                                       select file;

                    using (var libdb = new Libdb(true))
                    {
                        foreach (var file in files)
                        {
                            string fullpath = file.File;
                            int tmpid = -1;

                            // Fullpathでチェック
                            libdb.Sql = @"select id from library where path = '{0}'".Formatex(Tvdb.SqlEncode(fullpath));
                            using (var t = libdb.GetTable())
                                if (t.Read())
                                    tmpid = t.GetInt(0);

                            if (tmpid >= 0)
                            {
                                lock (libdb)
                                {
                                    libdb.Sql = @"update library set is_check=0, is_alive=0, update_at={1} where id={0}".Formatex(tmpid, DateTime.Now.Ticks);
                                    libdb.Execute();
                                }
                                continue;   //すでに登録済み
                            }

                            // 同一File名有り，Path違い →Pathを書き換え,終了
                            libdb.Sql = @"select id from library where file = '{0}'".Formatex(Tvdb.SqlEncode(Path.GetFileName(fullpath)));
                            using (var t = libdb.GetTable())
                                if (t.Read())
                                    tmpid = t.GetInt(0);

                            if (tmpid >= 0)
                            {
                                lock (libdb)
                                {
                                    libdb.Sql = @"update library set path='{2}', is_check=0, is_alive=0, update_at={1} where id={0}".Formatex(
                                        tmpid, 
                                        DateTime.Now.Ticks,
                                        Tvdb.SqlEncode(Path.GetFullPath(fullpath))
                                    );
                                    libdb.Execute();
                                }
                                Log.Info("修正: " + Path.GetFileName(fullpath));
                                continue;
                            }

                            token.ThrowIfCancellationRequested(); // Cancel時，ここで停止(2/3)

                            // 新規登録
                            try
                            {
                                FileInfo fi = new FileInfo(fullpath);
                                var med = new MediaLib();

                                if (Path.GetExtension(fullpath) == ".ts")
                                {
                                    var info = new TsFileInfo(fullpath);

                                    med.FilePath = fullpath;
                                    med.File = info.File; //med.File = Path.GetFileName(file);
                                    med.FileCreateDate = fi.CreationTime;
                                    med.FileModifiedDate =fi.LastWriteTime;
                                    med.FileSize = fi.Length;
                                    med.Duration = (int)(info.EndTime - info.StartTime).TotalSeconds;

                                    //med.MediaType = 10;
                                    med.Title = med.GetShowName(info.Title);
                                    med.SubTitle = med.GetSubTitle(info.Title);
                                    med.ServiceName = info.Service;
                                    med.Desc = info.Desc;
                                    med.LongDesc = info.LongDesc;
                                    med.GenreText = info.Genre;
                                    med.Yomi = AppDefine.Yomi.GetYomi(info.File); // ファイル名ひらがな
                                    //med.Yomi = AppDefine.Yomi.GetYomi(info.Title).ToKatakana(); // ファイル名カタカナ
                                    med.Roma = new Roma().Conv(med.Yomi);
                                    med.Season = 10; // x10してます
                                    med.Episode = med.GetEpisodeNo(info.Title);
                                    med.AiredDate = info.ReserveStart;
                                    //med.PlayDate;
                                    //med.PlayCount = 0;
                                    //med.Rate = 0;
                                    //med.Remarks = "";
                                    //med.IsAlive = 0;

                                    // File名からサブタイトル分離
                                    if (med.SubTitle == "")
                                        med.SubTitle = med.GetSubTitle(Path.GetFileNameWithoutExtension(fullpath));

                                    med.Path2Title(fullpath); // 特定ファイルパス構造時の処理(TitleGroup, Season書換)

                                    med.Add(libdb);
                                    
                                }
                                else
                                {
                                    var info = new Mp4FileInfo(fullpath);

                                    med.FilePath = fullpath;
                                    med.File = info.File;
                                    med.FileCreateDate = fi.CreationTime;
                                    med.FileModifiedDate =fi.LastWriteTime;
                                    med.FileSize = fi.Length;
                                    med.Duration = (int)(info.EndTime - info.StartTime).TotalSeconds;

                                    //med.MediaType = 10;
                                    med.Title = info.Title;
                                    med.SubTitle = info.Subtitle;
                                    med.ServiceName = info.Service;
                                    med.Desc = info.Desc;
                                    med.LongDesc = info.LongDesc;
                                    med.GenreText = info.Genre;
                                    med.Yomi = info.Yomi != "" ? info.Yomi : AppDefine.Yomi.GetYomi(info.File); // ファイル名ひらがな
                                    if (med.Yomi == null)
                                        med.Yomi = "";

                                    //med.Yomi = info.Yomi != "" ? info.Yomi : AppDefine.Yomi.GetYomi(info.Title).ToKatakana(); // ファイル名カタカナ
                                    med.Roma = info.Roma != "" ? info.Roma : med.Yomi == "" ? "" : new Roma().Conv(med.Yomi);
                                    med.Season = info.Season;
                                    med.Episode = info.Episode;
                                    med.AiredDate = info.ReserveStart;
                                    //med.PlayDate;
                                    //med.PlayCount = 0;
                                    //med.Rate = 0;
                                    //med.Remarks = "";
                                    //med.IsAlive = 0;

                                    // File名から話数抽出
                                    var ep = med.GetEpisodeNo(Path.GetFileNameWithoutExtension(fullpath));
                                    if (med.Episode == 0)
                                        med.Episode = ep;
                                    else if (ep != 0)
                                        med.Episode = ep; // n.5話対応

                                    // File名からサブタイトル分離
                                    if (med.SubTitle == "")
                                        med.SubTitle = med.GetSubTitle(Path.GetFileNameWithoutExtension(fullpath));

                                    med.Path2Title(fullpath); // 特定ファイルパス構造時の処理(TitleGroup, Season書換)

                                    med.Add(libdb);
                                    
                                }

                                Log.Info("登録: " + Path.GetFileName(fullpath));
                            }
                            catch (Exception ex)
                            {
                                Log.Error("{0} を登録できませんでした。{1}".Formatex(Path.GetFileName(fullpath), ex.Message));
                            }
                        }
                    }
                }

                using (var libdb = new Libdb(true))
                {
                    lock (libdb)
                    {
                        // dbネタでファイルが無かった=> is_aliveを 1 にする
                        libdb.Sql = @"update library set is_alive = 1, is_check = 0, update_at = {0} where is_check = 1".Formatex(DateTime.Now.Ticks);
                        libdb.Execute();
                    }
                }

                // Thumbnail生成
                Log.Info("Thumbnail生成開始。");
                using (var libdb = new Libdb(true))
                    for (var i = 1; i < 3; i++)
                    {
                        var op = Util.GetUserPath("thumblib");
                        libdb.Sql = @"select id, path, duration, create_at"
                                + " from library where is_alive = 0"
                                + " order by file_modified desc";
                        using (var t = libdb.GetTable())
                            while (t.Read())
                            {
                                token.ThrowIfCancellationRequested(); // Cancel時，ここで停止(3/3)

                                var id = t.GetInt(0);
                                var pa = t.GetStr(1);
                                var du = t.GetInt(2);
                                var ca = new DateTime(t.GetLong(3));
                                int margin = AppDefine.Main.Data["thumbnail.start"].ToInt();

                                FileInfo fi = new FileInfo(op + "\\" + id.ToString() + (i == 1 ? ".jpg" : ".m.jpg"));
                                if (ca > fi.LastWriteTime) // DB登録日より古いサムネは作り直し
                                    Util.MakeThumbnail(pa, id, margin, du, i, op);
                            }
                    }

                Log.Info("ライブラリ登録が完了しました。");

            }, token, TaskCreationOptions.AttachedToParent, TaskScheduler.Default)
            .ContinueWith(t =>
            {
                libInfoRunning = false;

                _addlibtokenSource.Dispose();
                _addlibtokenSource = null;

                if (t.IsCanceled)
                {
                    using (var libdb = new Libdb(true))
                    {
                        lock (libdb)
                        {
                            libdb.Sql = @"update library set is_check = 0";
                            libdb.Execute();
                        }
                    }
                    Log.Info("ライブラリ登録が停止しました。");
                }
            });            
        }

        public void CancelAddLibInfo() // ライブラリ登録処理を停止
        {
            // if (_tokenSource != null) _tokenSource.Cancel(true); //即死            
            if (_addlibtokenSource != null) _addlibtokenSource.Cancel();

            Log.Info("Library登録処理の停止をユーザー要求しました。");
        }

        public void SetPlayCount() // PlayCount / Play Date書き込み
        {
            var id = GetQuery("id").ToInt();
            int cnt = 1;

            using (var libdb = new Libdb(true))
            {
                libdb.Sql = @"select play_count from library where id={0}".Formatex(id);
                var x = libdb.GetTable();
                if (x.Read())
                    cnt = x.GetInt(0) + 1;
            }

            using (var libdb = new Libdb(true))
            {
                try
                {
                    libdb.BeginTrans();

                    libdb.Sql = @"update library set play_date='{1}', play_count={2} where id={0}".Formatex(
                        id,
                        DateTime.Now.Ticks,
                        cnt
                        );
                    libdb.Execute();
                    libdb.Commit();
                }
                catch
                {
                    libdb.Rollback();
                    throw;
                }
            }
        }

        public void SetRate() // SetRate // レート(★印の数 ０～５)
        {
            var id = GetQuery("id").ToInt();
            var rate = GetQuery("rate").ToInt();

            using (var libdb = new Libdb(true))
            {
                try
                {
                    libdb.BeginTrans();

                    libdb.Sql = @"update library set rate={1}, update_at={2} where id={0}".Formatex(
                        id,
                        rate,
                        DateTime.Now.Ticks
                    );

                    libdb.Execute();
                    libdb.Commit();
                }
                catch
                {
                    libdb.Rollback();
                    throw;
                }
            }
        }

        public void SetLibRemarks() // SetRemarks // 備考欄(手入力コメント)
        {
            var id = GetQuery("id").ToInt();
            var remarks = GetQuery("text").ToString();

            using (var libdb = new Libdb(true))
            {
                try
                {
                    libdb.BeginTrans();

                    libdb.Sql = @"update library set remarks='{1}', update_at={2} where id={0}".Formatex(
                        id,
                        Libdb.SqlEncode(remarks),
                        DateTime.Now.Ticks
                        );
                    libdb.Execute();

                    libdb.Commit();
                }
                catch
                {
                    libdb.Rollback();
                    throw;
                }
            }
        }

        public void SetTitleGroup() // ライブラリの(疑似)フォルダ名修正
        {
            var id = GetQuery("id").ToInt();
            var titlegroup = GetQuery("text").ToString();

            using (var libdb = new Libdb(true))
            {
                try
                {
                    libdb.BeginTrans();

                    libdb.Sql = @"update library set title_group='{1}', update_at={2} where id={0}".Formatex(
                        id,
                        Libdb.SqlEncode(titlegroup),
                        DateTime.Now.Ticks
                        );
                    libdb.Execute();

                    libdb.Commit();
                }
                catch
                {
                    libdb.Rollback();
                    throw;
                }
            }
        }

        public void CheckRemoveLibFile() // ファイル削除前に "ごみ箱" が使えるか確認する
        {
            var id = GetQuery("id").ToInt();

            string path = "";
            var fsize = 0;
            using (var libdb = new Libdb(true))
            {
                libdb.Sql = @"select path, file_size from library where id = {0}".Formatex(id);
                var t = libdb.GetTable();
                if (t.Read())
                {
                    path = t.GetStr(0);
                    fsize = t.GetInt(1);
                }
            }

            DirectoryInfo d = new DirectoryInfo(path);
            DriveInfo drive = new DriveInfo(d.Root.ToString());
            if (drive.DriveType == DriveType.Network)
            {
                ret.SetCode(1, "NG"); // Network Volume
                return;
            }
            
            string rp = "";
            do
            {
                if (d.Attributes == (System.IO.FileAttributes.Directory | System.IO.FileAttributes.ReparsePoint))
                {
                    rp = d.FullName;
                    break;
                }
                d = d.Parent;
            } while (d.Parent != null);

            if (rp == "")
                rp = d.Root.ToString();

            string guid = @"";
            var rm = Regex.Match(ShellFile.GetVolumeGuid(rp + @"\"), @"{([A-Fa-f0-9]{8})\-([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{12})}");
            if (rm != null)
                guid = rm.Groups[0].Value;
            else
            {
                ret.SetCode(2, "Caution");
                return;
            }

            var tfs = (int)ShellFile.GetTrashFreeSpace(rp);

            string regkey = @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\BitBucket\Volume\";
            int tmc = (int)Microsoft.Win32.Registry.GetValue(regkey + guid, @"MaxCapacity", 0) * 1000;

            if (tmc - tfs > 0)
                ret.SetCode(0, "OK"); 
            else
                ret.SetCode(1, "NG"); // ごみ箱いっぱい！
        }

        public void RemoveLibFile() // ライブラリのファイルを削除
        {
            var id = GetQuery("id").ToInt();

            string path = "";
            using (var libdb = new Libdb(true))
            {
                libdb.Sql = @"select path from library where id = {0}".Formatex(id);
                var t = libdb.GetTable();
                    if (t.Read())
                        path = t.GetStr(0);
            }
            
            if (File.Exists(path))
            {
                ShellFile.Delete(path);
                Util.RemoveThumbnail(id, Util.GetUserPath("thumblib"));

                using (var libdb = new Libdb(true))
                {
                    libdb.BeginTrans();
                    libdb.Sql = @"update library set is_alive=1, update_at={1} where id ={0}".Formatex(
                        id,
                        DateTime.Now.Ticks
                    );
                    libdb.Execute();
                    libdb.Commit();
                }
            }
        }

        public void ClearLibrary() // ライブラリ全消し(DBとサムネのみ)
        {
            using (var libdb = new Libdb(true))
            {
                libdb.Sql = "delete from library";
                libdb.Execute();
            }
            var file = Util.GetUserPath("thumblib") + "\\*.jpg";
            ShellFile.Delete(file);
            Log.Info("ライブラリをクリアしました。");
        }

        public void RemoveLibInactFiles() // "is_alive = 1"をDBから削除します(mod14.2)
        {
            using (var libdb = new Libdb(true))
            {
                libdb.Sql = @"select id from library where is_alive=1";
                var t = libdb.GetList();
                foreach (var x in t)
                {
                    var id = x[0].ToString().ToInt();
                    Util.RemoveThumbnail(id, Util.GetUserPath("thumblib")); // サムネ削除
                }

                if (t.Count > 0)
                {
                    libdb.BeginTrans();
                    libdb.Sql = @"delete from library where is_alive=1";
                    libdb.Execute();
                    libdb.Commit();

                    //libdb.Vacuum();
                }

                ret.data1 = t.Count;
            }
        }


        // 予約有効無効の切り替え(トグルします.Timekeeper用)
        public void ChangeReserveState()
        {
            var id = GetQuery("id");

            using (var tvdb = new Tvdb(true))
            {
                var res = new Reserve(tvdb, id.ToInt());
                if (((Reserve.StatusCode)Enum.ToObject(typeof(Reserve.StatusCode), res.Status) & Reserve.StatusCode.Enable) == Reserve.StatusCode.Enable)
                    res.SetEnable(tvdb, false);
                else
                    res.SetEnable(tvdb, true);
            }
        }


        // kodi設定の有無確認
        public void UseKodi()
        {
            var url = AppDefine.Main.Data["kodi.url"];
            if (url == "")
                ret.SetCode(1, "NoUse");
            else
                ret.SetCode(0, "Use");
        }

        // kodi Web UIのURLを返す
        public void GetKodiUI()
        {
            var url = AppDefine.Main.Data["kodi.url"];
            if (url == "")
            {
                ret.SetCode(1, "kodi.urlが設定されていません。");
                return;
            }

            var id = GetQuery("id");
            var target = GetQuery("target");
            var path = GetKodiPath(target, id, true);

            // sample
            // http://osmc.local/#browser/video/smb%3A%2F%2Fhdr13.local%2Fmnt%2Fe3TB1%2FTV%20Show%2Fcitrus%2FSeason%201%2F

            ret.data1 = url + "#browser/video/" + path;
        }

        // 外部のkodiへ再生/Playlist追加指示する
        public void PlayByKodi()
        {
            var url = AppDefine.Main.Data["kodi.url"];
            if (url == "")
            {
                ret.SetCode(1, "kodi.urlが設定されていません。");
                return;
            }

            var id = GetQuery("id");
            var target = GetQuery("target");
            var play = GetQuery("play") == "true" ? true : false;

            var path = GetKodiPath(target, id);

            if (play) 
                url = url + "jsonrpc?request={%22jsonrpc%22:%222.0%22,%22id%22:1,%22method%22:%22Player.Open%22,%22params%22:{%22item%22:{%22file%22:%22" + path + "%22}}}";
            else
                url = url + "jsonrpc?request={%22jsonrpc%22:%222.0%22,%22id%22:1,%22method%22:%22Playlist.Add%22,%22params%22:{%22playlistid%22:1,%22item%22:{%22file%22:%22" + path + "%22}}}";

            // kick kodi
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            try
            {
                wc.DownloadStringCompleted += PlayByKodiProc1;
                wc.DownloadStringAsync(new Uri(url));
            }
            catch (WebException ex)
            {
                ret.SetCode(1, ex.Message);
            }
        }

        // "smb://～"なファイルパス生成
        private string GetKodiPath(string target, string id, bool web = false)
        {
            var dbg = false;

            var path = "";

            if (target == "library")
            {
                using (var libdb = new Libdb(true))
                {
                    libdb.Sql = @"select [path] from library where id = {0}".Formatex(id);
                    path = libdb.GetData().ToString();
                }
            }
            else
            {
                // for record
                using (var tvdb = new Tvdb(true))
                {
                    tvdb.Sql = @"select file from record where id = {0}".Formatex(id);
                    path = tvdb.GetData().ToString();
                }

                var t = FindFile.SearchFile(AppDefine.Main.Data["record.folder"], path, false);
                path = t[0];
            }

            // for kodi WebUI ?
            if (web)
            {
                path = Path.GetDirectoryName(path);
                if (path.Substring(path.Length - 1, 1) != "\\")
                    path = path + "\\";
            }

            // Get Share Folder
            ManagementScope scope = new ManagementScope(); // 自分の"\\root\\cimv2"
            ObjectQuery query = new ObjectQuery("Select * From Win32_Share where Type = 0");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection collection = searcher.Get();

            foreach (ManagementObject managementObject in collection)
            {
                var x = string.Format(managementObject["Path"].ToString());
                x = x.Substring(x.Length - 1, 1) == "\\" ? x.Substring(0, x.Length - 1) : x; // x:\ 直下対策
                
                path = path.Replace(x, string.Format(managementObject["Name"].ToString()));
            }

            if (dbg)
                path = Uri.EscapeDataString(path.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar).Replace("L:/", "smb://hdr13.local/mnt/")); // for test env.
            else
                path = Uri.EscapeDataString(
                    "smb://" + Environment.MachineName.ToLower() + (Regex.IsMatch(AppDefine.Main.Data["kodi.url"],".local") ? ".local/" : "/")
                    + path.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                    );

            return path;
        }

        private void PlayByKodiProc1(Object sender, DownloadStringCompletedEventArgs e)
        {
            try { 
                var data = DynamicJson.Parse(e.Result);
                if (data.IsDefined("error"))
                    ret.SetCode(1, data.error.message);
                else
                    ret.SetCode(0, "送ったよ！");
            }
            catch (System.Reflection.TargetInvocationException ee)
            {
                ret.SetCode(1, ee.InnerException.Message);
            }
        }


        // for Google Calendar
        public void UpdateGCalendar()
        {
            string CalendarId = AppDefine.Main.Data["google.calendarid"];
            ret.code = 1;

            if (AppDefine.Main.Data["google.calendar"] != "on")
                ret.message = "Google Calendarの設定がONになっていません";
            else if (CalendarId == "")
                ret.message = "Google CalendarIdが設定されていません";
            else
            {
                ret.code = 0;
                GCalendar.UpdateEvents(CalendarId);
            }
        }
        
        public void DelAllGCalendar()
        {
            string CalendarId = AppDefine.Main.Data["google.calendarid"];
            ret.code = 1;

            if (AppDefine.Main.Data["google.calendar"] != "on")
                ret.message = "Google Calendarの設定がONになっていません";
            else if (CalendarId == "")
                ret.message = "Google CalendarIdが設定されていません";
            else
            {
                ret.code = 0;
                GCalendar.DelAllEvents(CalendarId);
            }
        }

        // keii ---

    }

    public static class ShellFile
    {
        public static void Delete(string path)
        {
            var sh = new SHFILEOPSTRUCT
            {
                hwnd = IntPtr.Zero,
                wFunc = FOFunc.FO_DELETE,
                pFrom = path + "\0\0",
                pTo = null,
                fFlags = FOFlags.FOF_SILENT | FOFlags.FOF_NOERRORUI | FOFlags.FOF_ALLOWUNDO | FOFlags.FOF_NOCONFIRMATION,
                fAnyOperationsAborted = false,
                hNameMappings = IntPtr.Zero,
                lpszProgressTitle = ""
            };

            var ret = SHFileOperation(ref sh);

            if (ret != 0)
                throw new Exception("ファイルの削除に失敗しました。");
        }

        enum FOFunc : uint
        {
            FO_MOVE = 0x0001,
            FO_COPY = 0x0002,
            FO_DELETE = 0x0003,
            FO_RENAME = 0x0004
        }

        enum FOFlags : ushort
        {
            FOF_MULTIDESTFILES = 0x0001,
            FOF_CONFIRMMOUSE = 0x0002,
            FOF_SILENT = 0x0004,            // don't create progress/report
            FOF_RENAMEONCOLLISION = 0x0008,
            FOF_NOCONFIRMATION = 0x0010,    // Don't prompt the user.
            FOF_WANTMAPPINGHANDLE = 0x0020, // Fill in SHFILEOPSTRUCT.hNameMappings
                                            // Must be freed using SHFreeNameMappings
            FOF_ALLOWUNDO = 0x0040,
            FOF_FILESONLY = 0x0080,         // on *.*, do only files
            FOF_SIMPLEPROGRESS = 0x0100,    // means don't show names of files
            FOF_NOCONFIRMMKDIR = 0x0200,    // don't confirm making any needed dirs
            FOF_NOERRORUI = 0x0400,         // don't put up error UI
            FOF_NOCOPYSECURITYATTRIBS = 0x0800,  // dont copy NT file Security Attributes
            FOF_NORECURSION = 0x1000,       // don't recurse into directories.
            FOF_NO_CONNECTED_ELEMENTS = 0x2000,  // don't operate on connected elements.
            FOF_WANTNUKEWARNING = 0x4000,   // during delete operation, warn if nuking instead of recycling (partially overrides FOF_NOCONFIRMATION)
            FOF_NORECURSEREPARSE = 0x8000   // treat reparse points as objects, not containers
        }

        [StructLayout(LayoutKind.Sequential)]
        struct SHFILEOPSTRUCT
        {
            public IntPtr hwnd;
            public FOFunc wFunc;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pFrom;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pTo;
            public FOFlags fFlags;
            public bool fAnyOperationsAborted;
            public IntPtr hNameMappings;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpszProgressTitle;
        }

        [DllImport("Shell32.dll", CharSet = CharSet.Unicode)]
        static extern int SHFileOperation(ref SHFILEOPSTRUCT lpFileOp);

        
        public static ulong GetTrashFreeSpace(string drive)
        {
            uint result;

            LPSHQUERYRBINFO info = new LPSHQUERYRBINFO();
            info.cbSize = (uint)Marshal.SizeOf(info);

            result = SHQueryRecycleBin(drive, ref info);
            if (result == 0)
                return info.i64Size;
            else
                return 0;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        internal struct LPSHQUERYRBINFO
        {
            public uint cbSize;
            public ulong i64Size;
            public ulong i64NumItems;
        }

        [DllImport("shell32.dll", CharSet = CharSet.Unicode)]
        internal static extern uint SHQueryRecycleBin(
            string pszRootPath,
            [MarshalAs(UnmanagedType.Struct)]ref LPSHQUERYRBINFO pSHQueryRBInfo);


        public static string GetVolumeGuid(string MountPoint)
        {
            const int MaxVolumeNameLength = 100;
            System.Text.StringBuilder sb = new System.Text.StringBuilder(MaxVolumeNameLength);
            if (!GetVolumeNameForVolumeMountPoint(MountPoint, sb, (uint)MaxVolumeNameLength))
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            return sb.ToString();
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool GetVolumeNameForVolumeMountPoint(
            string lpszVolumeMountPoint, 
            [Out] System.Text.StringBuilder lpszVolumeName,
            uint cchBufferLength);
        
    }

    // keii --
    public class FileListArg
    {
        public string File { get; set; }
        public DateTime Date { get; set; }
    }

    public class FindFile
    {
        public static List<FileListArg> GetAllFilesWithDate(FileListArg DirPath, bool TopDirectoryOnly = false)
        {
            List<FileListArg> lstStr = new List<FileListArg>();
            Regex regex = new Regex(@"\$Recycle\.Bin|System\sVolume\sInformation|Documents\sand\sSettings|Recovery|ProgramData", RegexOptions.IgnoreCase);

            try
            {
                foreach (String s in Directory.GetFiles(DirPath.File))
                {
                    if (!regex.IsMatch(s))
                    {
                        var fi = new FileInfo(s);
                        var x = new FileListArg
                        {
                            File = s,
                            Date = fi.LastWriteTime
                        };
                        lstStr.Add(x);
                    }
                }

                if (TopDirectoryOnly)
                    return lstStr;

                foreach (String s in Directory.GetDirectories(DirPath.File))
                {
                    if (regex.IsMatch(s))
                        continue;

                    FileListArg xs = new FileListArg
                    {
                        File = s,
                        Date = new DateTime()
                    };

                    List<FileListArg> lstBuff = GetAllFilesWithDate(xs);
                    lstBuff.ForEach(delegate (FileListArg str)
                    {
                        if (!regex.IsMatch(str.File))
                        {
                            var fi = new FileInfo(str.File);
                            var xx = new FileListArg
                            {
                                File = str.File,
                                Date = fi.LastWriteTime
                            };
                            lstStr.Add(xx);
                        }
                    });
                }
            }
            catch (UnauthorizedAccessException)
            {
                // アクセスできなかったので無視
                //Log.Error("Unauthorized Access Exception !: " + ex.Message);
                //Log.Debug(ex.StackTrace);
            }
            catch (DirectoryNotFoundException)
            {
                //Log.Error("Directory NotFound Exception !: " + ex.Message);
                //Log.Debug(ex.StackTrace);
            }

            return lstStr;
        }

        public static List<String> GetAllFiles(String DirPath)
        {
            List<String> lstStr = new List<String>();
            Regex regex = new Regex(@"\$Recycle\.Bin|System\sVolume\sInformation|Documents\sand\sSettings|Recovery|ProgramData", RegexOptions.IgnoreCase);

            try
            {
                foreach (String s in Directory.GetFiles(DirPath))
                {
                    if (!regex.IsMatch(s))
                        lstStr.Add(s);
                }

                foreach (String s in Directory.GetDirectories(DirPath))
                {
                    if (regex.IsMatch(s))
                        continue;

                    List<String> lstBuff = GetAllFiles(s);
                    lstBuff.ForEach(delegate (String str)
                    {
                        if (!regex.IsMatch(str))
                            lstStr.Add(str);
                    });
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                // アクセスできなかったので無視
                Log.Error("Unauthorized Access Exception !: " + ex.Message);
                //Log.Debug(ex.StackTrace);
            }
            catch (DirectoryNotFoundException ex)
            {
                Log.Error("Directory NotFound Exception !: " + ex.Message);
                //Log.Debug(ex.StackTrace);
            }

            return lstStr;
        }

        public static List<String> SearchFile(String SearchDir, String FileName, bool TopDirectoryOnly = false)
        {
            List<String> lstStr = new List<String>();
            Regex regex = new Regex(@"\$Recycle\.Bin|System\sVolume\sInformation|Documents\sand\sSettings|Recovery|ProgramData", RegexOptions.IgnoreCase);
            Regex regfile = new Regex(Regex.Escape(FileName));

            try
            {
                foreach (String s in Directory.GetFiles(SearchDir))
                {
                    if (regex.IsMatch(s))
                        continue;

                    if (regfile.IsMatch(s))
                    {
                        lstStr.Add(s);
                        break;
                    }
                }

                if (TopDirectoryOnly)
                    return lstStr;

                if (lstStr.Count == 0)
                {
                    foreach (String s in Directory.GetDirectories(SearchDir))
                    {
                        if (regex.IsMatch(s))
                            continue;

                        List<String> lstBuff = SearchFile(s, FileName);
                        lstBuff.ForEach(delegate (String str)
                        {
                            if (!regex.IsMatch(str))
                            {
                                if (regfile.IsMatch(str))
                                {
                                    lstStr.Add(str);
                                }
                            }
                        });
                    }
                }

            }
            catch (UnauthorizedAccessException)
            {
                // アクセスできなかったので無視
            }
            catch (DirectoryNotFoundException)
            {

            }

            return lstStr;

        }
    }
    // keii --
}