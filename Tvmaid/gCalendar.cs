﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
//using Google.Apis.Calendar.v3.Data; // Tvmaid.Eventと被る
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Tvmaid
{
    class GCalendarData
    {
        public int Id;
        public string Fsid; // ExtendedPropertiesData
        public string Eid; // ExtendedPropertiesData
        public DateTime Start;
        public DateTime End;
        public string Title; // Summary 
        public string Desc;
        public string LongDesc;
        public string GenreText;
        public string ServiceName; // Location
        
        public GCalendarData() { }

        public GCalendarData(DbTable t)
        {
            init(t);
        }
        
        void init(DbTable t)
        {
            Id = t.GetInt("id");
            Fsid = t.GetLong("fsid").ToString();
            Eid = t.GetInt("eid").ToString();
            Start = new DateTime(t.GetLong("start"));
            End = new DateTime(t.GetLong("end"));

            Title = t.GetStr("title"); //Summary
            Desc = t.GetStr("desc");
            LongDesc = t.GetStr("longdesc");
            //GenreText = t.GetStr("genre_text");
            //GenreText = t.GetStr("genre_text").Substring(0, t.GetStr("genre_text").Length - 1);
            GenreText = t.GetStr("genre_text").Replace("\n","");
            ServiceName = t.GetStr("service_name");            
        }
    }
    
    class GCalendar
    {
        static string ApplicationName = "Tvmaid Reserve Informations.";
        public string CalendarId = "";

        public string ColorId = ""; // 1 - 11(https://developers.google.com/calendar/v3/reference/colors/get のEvent)

        public static string timezone = "Asia/Tokyo";
        public static string localoffset = "+09:00";

        public Google.Apis.Calendar.v3.CalendarService gService = null;
        public Google.Apis.Calendar.v3.EventsResource gEvents = null;
        public Google.Apis.Calendar.v3.Data.Events EventList;
        public Google.Apis.Calendar.v3.Data.Event EventData;

        static string[] Scopes = {
            CalendarService.Scope.Calendar
            //,CalendarService.Scope.CalendarReadonly
        };

 
        public GCalendar() { }

        public GCalendar(string Calendar_Id)
        {
            CalendarId = Calendar_Id;
            UserCredential gAcl = GetUserCredential();
            gService = GetCalendarService(gAcl);
            gEvents = gService.Events;
        }

        public void Close()
        {
            EventList = null;
            EventData = null;
            gEvents = null;
            gService = null;
            //gAcl = null;
        }

        static UserCredential GetUserCredential()
        {
            UserCredential credential = null;

            try
            {
                using (var stream = new FileStream(Util.GetUserPath("credentials.json"), FileMode.Open, FileAccess.Read))
                {
                    string credPath = Path.Combine(Util.GetUserPath(), "token.json");
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        Scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    //Console.WriteLine("Credential file saved to: " + credPath);
                    //Log.Info("Credential file saved to: " + credPath);
                }
            }
            catch (Exception ex)
            {
                credential = null;
                Log.Error("Google Calendarの認証に失敗しました。 " + ex.Message);
            }

            return credential;
        }

        static CalendarService GetCalendarService(UserCredential credential)
        {
            CalendarService calendarService = null;

            try
            {
                calendarService = new CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName
                });
                //Log.Info("Google Calendar: Make Service.");
            }
            catch (Exception ex)
            {
                calendarService = null;
                Log.Error("Google Calendarのサービス生成に失敗しました。 " + ex.Message);

            }
            return calendarService;
        }

        public void AddNewEvent()
        {
            try
            {
                Google.Apis.Calendar.v3.EventsResource.InsertRequest request = gEvents.Insert(EventData, CalendarId);
                Google.Apis.Calendar.v3.Data.Event createdEvent = request.Execute();
            }
            catch (Exception ex)
            {
                Log.Error("Google Calendarへ新規登録に失敗しました。 " + ex.Message);
            }

        }

        public void UpdateEvent()
        {
            try
            {
                Google.Apis.Calendar.v3.EventsResource.UpdateRequest request = gEvents.Update(EventData, CalendarId, EventData.Id);
                Google.Apis.Calendar.v3.Data.Event UpdateEvent = request.Execute();
            }
            catch (Exception ex)
            {
                Log.Error("Google Calendarnの更新に失敗しました。 " + ex.Message);
            }

        }

        public void DeleteEvent()
        {
            try
            {
                Google.Apis.Calendar.v3.EventsResource.DeleteRequest request = gEvents.Delete(CalendarId, EventData.Id);
                var DeleteEvent = request.Execute();
            }
            catch (Exception ex)
            {
                Log.Error("Google Calendarnの削除に失敗しました。 " + ex.Message);
            }

        }

        public void GetEvents(DateTime TimeMin)
        {
            try
            {
                EventsResource.ListRequest request = gEvents.List(CalendarId);
                request.TimeMin = TimeMin;
                request.ShowDeleted = false;
                request.SingleEvents = true; // OrderBy する時 true 必須
                //request.MaxResults = 10;
                request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                EventList = request.Execute();
            }
            catch (Exception ex)
            {
                Log.Error("Google Calendarnの情報取得に失敗しました。 " + ex.Message);
            }
        }

        public void GetEvent(string EventId)
        {
            try
            {
                EventsResource.GetRequest request = gEvents.Get(CalendarId, EventId);
                EventData = request.Execute();
            }
            catch (Exception ex)
            {
                Log.Error("Google Calendarnの情報取得に失敗しました。 " + ex.Message);
            }
        }

        // ----

        public long GetReserveTimeMin()
        {
            using (var tvdb = new Tvdb(true))
            {
                //tvdb.Sql = "select start from reserve where status & {0} order by start limit 1".Formatex((int)Reserve.StatusCode.Enable);
                tvdb.Sql = "select start from reserve order by start limit 1";
                using (var t = tvdb.GetTable())
                    return t.Read() ? t.GetLong(0) : DateTime.Now.Ticks;
            }
        }

        public Google.Apis.Calendar.v3.Data.EventDateTime Dt2edt(DateTime s)
        {
            var x = new Google.Apis.Calendar.v3.Data.EventDateTime()
            {
                DateTime = s,
                TimeZone = GCalendar.timezone
            };
            return x;
        }

        public void SetEventData(GCalendar gc, GCalendarData s)
        {
            string convColorId(string desc)
            {
                string col = "8"; // Google Calendar Event ColorId (8 = #e1e1e1...Gray)

                PairList list = new PairList(Util.GetUserPath("gcalcolor.def"));
                list.Load();

                foreach (KeyValuePair<string, string> p in list)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(desc, System.Text.RegularExpressions.Regex.Escape(p.Key)))
                    {
                        col = p.Value;
                        break;
                    }
                }
                return col;
            }

            gc.EventData = new Google.Apis.Calendar.v3.Data.Event()
            {
                Summary = s.Title,
                Location = s.ServiceName,
                Description = s.Desc + "\r\n" + s.LongDesc + "\r\n\r\n" + s.GenreText,
                Start = Dt2edt(s.Start),
                End = Dt2edt(s.End),
                Transparency = "transparent", // "予定なし"
                ColorId = convColorId(s.GenreText)
            };

            gc.EventData.ExtendedProperties = new Google.Apis.Calendar.v3.Data.Event.ExtendedPropertiesData
            {
                Private__ = new Dictionary<string, string>() {
                    { "fsid", s.Fsid },
                    { "eid", s.Eid }
                }
            };
        }

        static string CheckSetting()
        {
            if (AppDefine.Main.Data["google.calendar"] != "on")
            {
                Log.Alert("Google Calendar設定が OFF です。");
                return "";
            }

            string CalendarId = AppDefine.Main.Data["google.calendarid"];
            if (CalendarId == "")
            {
                Log.Alert("Google CalendarIdが設定されていません。");
                return "";
            }
            return CalendarId;
        }

        public static void DelAllEvents()
        {
            string CalendarId = CheckSetting();
            if (CalendarId == "")
                return;

            DelAllEvents(CalendarId);
        }

        public static void DelAllEvents(string CalendarId)
        {
            Log.Info("Google Calendar: Kick Manual Delete All.");

            Task.Run(() =>
            {
                GCalendar gcal = new GCalendar(CalendarId);

                // Get Calendar "List"
                var x = DateTime.MinValue;
                gcal.GetEvents(x); //gcal.DataListにネタが格納される。

                foreach (Google.Apis.Calendar.v3.Data.Event item in gcal.EventList.Items)
                {
                    gcal.EventData = new Google.Apis.Calendar.v3.Data.Event() { Id = item.Id };
                    gcal.DeleteEvent();
                }

                gcal.Close();
                Log.Info("Google Calendar: Finish Delete All.");

            });

        }

        public static void UpdateEvents()
        {
            string CalendarId = CheckSetting();
            if (CalendarId == "")
                return;

            UpdateEvents(CalendarId);
        }

        public static void UpdateEvents(string CalendarId)
        {
            Log.Info("Google Calendar: Start Update.");

            Task.Run(() =>
            {
                GCalendar gcal = new GCalendar(CalendarId);

                // Get Calendar "List"
                var x = new DateTime(gcal.GetReserveTimeMin()); // 一番近い予約の日時を取得
                gcal.GetEvents(x);                              // gcal.DataListにネタが格納される。

                var list = new List<GCalendarData>();

                using (var tvdb = new Tvdb(true))
                {
                    tvdb.Sql = "select e.id, e.fsid, e.eid, e.start, e.end, e.title, e.desc, e.longdesc, e.genre_text, sv.name as service_name"
                            + " from reserve r inner join event e on r.fsid=e.fsid and r.eid=e.eid"
                            + " inner join (select fsid, name from service group by fsid) sv on e.fsid = sv.fsid"
                            + " where r.status & 1 > 0"
                            + " order by r.start";

                    using (var t = tvdb.GetTable())
                        while (t.Read())
                            list.Add(new GCalendarData(t));
                }

                if (gcal.EventList.Items != null && gcal.EventList.Items.Count > 0)
                {
                    // Tvmaid -> GCalendar
                    foreach (var s in list)
                    {
                        var fsid = s.Fsid.ToString();
                        var eid = s.Eid.ToString();
                        var flg = false;

                        foreach (Google.Apis.Calendar.v3.Data.Event item in gcal.EventList.Items)
                        {
                            // fsidが空なので，Deleteする
                            if (item.ExtendedProperties.Private__.ContainsKey("fsid") == false)
                                if (item.ExtendedProperties.Private__.ContainsKey("check") == false)
                                {
                                    gcal.EventData = new Google.Apis.Calendar.v3.Data.Event()
                                    {
                                        Id = item.Id
                                    };

                                    gcal.DeleteEvent();

                                    item.ExtendedProperties.Private__.Add("check", "D");
                                    flg = true;
                                    continue;
                                }

                            if (item.ExtendedProperties.Private__.ContainsKey("check") == true)
                                continue;

                            // update
                            if (item.ExtendedProperties.Private__["fsid"] == fsid && item.ExtendedProperties.Private__["eid"] == eid)
                            {
                                if (item.Description != s.Desc + "\r\n" + s.LongDesc + "\r\n\r\n" + s.GenreText)
                                {
                                    gcal.SetEventData(gcal, s);
                                    gcal.EventData.Id = item.Id;
                                    gcal.UpdateEvent();

                                    item.ExtendedProperties.Private__.Add("check", "U");
                                    flg = true;
                                    break;

                                }
                                item.ExtendedProperties.Private__.Add("check", "C");
                                flg = true;
                                break;
                            }

                        }

                        if (flg == false)
                        {
                            //  AddNew
                            gcal.SetEventData(gcal, s);
                            gcal.AddNewEvent();
                        }

                    }

                    foreach (Google.Apis.Calendar.v3.Data.Event item in gcal.EventList.Items)
                    {
                        // Tvmaidで予約が無くなったものを削除
                        if (item.ExtendedProperties.Private__.ContainsKey("check") == false)
                        {
                            gcal.EventData = new Google.Apis.Calendar.v3.Data.Event() { Id = item.Id };
                            gcal.DeleteEvent();
                        }
                    }
                }
                else
                {
                    // Add AllNew
                    foreach (var s in list)
                    {
                        gcal.SetEventData(gcal, s);
                        gcal.AddNewEvent();
                    }
                }

                gcal.Close();
                Log.Info("Google Calendar: Finish Update.");

            });

        }

    }
}
