﻿using CoreTweet;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Tvmaid
{
    public class Twitter
    {
        // api
        /*        
        static string api_key = AppDefine.Main.Data["tweet.api_key"];
        static string api_secret = AppDefine.Main.Data["tweet.api_secret"];
        static string accesstoken = AppDefine.Main.Data["tweet.accesstoken"];
        static string accesstokensecret = AppDefine.Main.Data["tweet.accesstokensecret"];
        */
        

        public static void Certification(string key, string secert)
        {
            string pincode;
            string api_key = AppDefine.Main.Data["tweet.api_key"];
            string api_secret = AppDefine.Main.Data["tweet.api_secret"];
            //string accesstoken = AppDefine.Main.Data["tweet.accesstoken"];
            //string accesstokensecret = AppDefine.Main.Data["tweet.accesstokensecret"];

            if (key.Length != 0 && secert.Length != 0)
            {
                api_key = key;
                api_secret = secert;
            }
            else
            {
                if (api_key == null || api_secret == null)
                {
                    Log.Error("API Key/API Secretが設定されていません。");
                    return;
                }
                else if (api_key.Length == 0 || api_secret.Length == 0)
                {
                    Log.Error("API Key/API Secretが設定されていません。");
                    return;
                }
            }

            // セッション開始
            var session = OAuth.Authorize(api_key, api_secret);
            Process.Start(session.AuthorizeUri.AbsoluteUri); // 認証ページをブラウザで開く (このページでPINCODEをコピーしてくる)

            // PINCODE入力欄表示
            Tvmaid.Gui.Pincode f = new Tvmaid.Gui.Pincode();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pincode = f.ResultText;
            } else {
                return;
            }

            // トークン取得
            var certtokens = OAuth.GetTokens(session, pincode);

            AppDefine.Main.Data["tweet.accesstoken"] = certtokens.AccessToken;
            AppDefine.Main.Data["tweet.accesstokensecret"] = certtokens.AccessTokenSecret;
            AppDefine.Main.Data["tweet.userid"] = (certtokens.UserId).ToString();
            AppDefine.Main.Data["tweet.username"] = certtokens.ScreenName;
            
            //AppDefine.Main.Save();
        }

        public static void Send(string body)
        {
            string api_key = AppDefine.Main.Data["tweet.api_key"];
            string api_secret = AppDefine.Main.Data["tweet.api_secret"];
            string accesstoken = AppDefine.Main.Data["tweet.accesstoken"];
            string accesstokensecret = AppDefine.Main.Data["tweet.accesstokensecret"];

            Task.Run(() =>
            {
                
                for (var i = 0; i < 100; i++) // Network再開待ち Timeout 100 x 100ms
                {
                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                    {                        
                        for (var j = 0; j < 60; j++) // DNSが引けるようになるのを待つ Timeout 60 x 1000ms
                        {
                            try
                            {
                                System.Net.NetworkInformation.Ping p = new System.Net.NetworkInformation.Ping();
                                System.Net.NetworkInformation.PingReply reply = p.Send("api.twitter.com", 1000); // pingして確認
                                p.Dispose();
                                if (reply.Status == System.Net.NetworkInformation.IPStatus.Success) break;
                            }
                            catch (System.Net.NetworkInformation.PingException)
                            {
                                //Log.Debug("Ping Test said: " + ex.Message);
                            }
                            Thread.Sleep(1000);
                        }

                        // Tweet
                        Tokens tokens = Tokens.Create(api_key, api_secret, accesstoken, accesstokensecret);
                        try
                        {
                            tokens.Statuses.Update(status => body);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("Twitter said: " + ex.Message);
                        }

                        return;
                    }
                    Thread.Sleep(100);
                }
            });
        }        
    }

}