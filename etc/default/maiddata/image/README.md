# 画像データについて

ここに収められている画像は，キャラ作成ツール [CHARAT CHOCO](https://charat.me/chibi/) で作成したものを加工しています。  
CHARAT CHOCOで生成される画像は，**全身**ですが，maidのフェイスアイコンなので顔の部分だけ抜き出して使用しています。

ご自身で作成する際は，もっと寄った(顔アップに)した方が，表情が見やすいと思います。

CHARAT CHOCO  
[https://charat.me/chibi/](https://charat.me/chibi/)  
ライセンス  
[https://charat.me/terms/](https://charat.me/terms/)
