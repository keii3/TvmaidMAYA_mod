# Tvmaid MAYA
Tvmaid MAYAは、Windows用録画予約プログラムです。  
TVTestを使用して、番組表の取得、録画を行います。

このプログラムは実験用で、暗号化されたデータを扱う機能はありません。  

## ライセンス
MITライセンスで公開します。  
This software is released under the MIT License, see LICENSE.txt.

## 他作者の著作物のライセンス
以下の著作物が組み込まれています。

### System.Data.SQLite
[https://system.data.sqlite.org/index.html/doc/trunk/www/index.wiki](https://system.data.sqlite.org/index.html/doc/trunk/www/index.wiki)  
Public domain.

### DynamicJson
created and maintained by neuecc \<ils@neue.cc>  
[https://dynamicjson.codeplex.com/](https://dynamicjson.codeplex.com/)  
licensed under Microsoft Public License(Ms-PL)

### jQuery
Copyright 2013 jQuery Foundation and other contributors  
[https://jquery.com/](https://jquery.com/)  
The MIT License  
[http://opensource.org/licenses/mit-license.php](http://opensource.org/licenses/mit-license.php)

### XDate
[http://arshaw.com/xdate/](http://arshaw.com/xdate/)  
Dual-licensed under MIT or GPL  
[https://github.com/arshaw/xdate/blob/master/MIT-LICENSE.txt](https://github.com/arshaw/xdate/blob/master/MIT-LICENSE.txt
)

### Riot.js
[http://riotjs.com/ja/](http://riotjs.com/ja/)  
MIT License  
[https://opensource.org/licenses/mit-license.php](https://opensource.org/licenses/mit-license.php)

### hls.js
Copyright (c) 2017 Dailymotion \(http://www.dailymotion.com)  
[https://github.com/video-dev/hls.js](https://github.com/video-dev/hls.js)
Apache 2.0 License  
[https://github.com/video-dev/hls.js/blob/master/LICENSE](https://github.com/video-dev/hls.js/blob/master/LICENSE)

### rplsinfo.exe
[http://saysaysay.net/rplstool/rplsinfo](http://saysaysay.net/rplstool/rplsinfo)  
[http://saysaysay.net/rplstool/rplsinfo#redistribution](http://saysaysay.net/rplstool/rplsinfo#redistribution)

### アプリケーションアイコン
ICOON MONO  
TopeconHeroes  
[http://icooon-mono.com/](http://icooon-mono.com/)
[http://icooon-mono.com/license](http://icooon-mono.com/license)

### Webアイコン
Material icons  
[https://material.io/icons/](https://material.io/icons/)
Apache 2.0 License

### Phonon Framework
CSSファイルを一部コピーして利用しています。  
The MIT License  
[https://github.com/quark-dev/Phonon-Framework/blob/master/LICENSE](https://github.com/quark-dev/Phonon-Framework/blob/master/LICENSE)

### niconico-ch.txt
NicoJKのjkch.sh.txtをコピーして利用しています。  
{https://github.com/xtne6f/NicoJK/releases](https://github.com/xtne6f/NicoJK/releases)

### TVTestPlugin.h
デジタル放送総合技術研究開発機構  
[https://github.com/DBCTRADO/TVTest](https://github.com/DBCTRADO/TVTest)

