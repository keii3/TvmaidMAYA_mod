# Tvmaid MAYA mod

TvmaidMAYAを好き勝手に弄りまわした物。  
詳細は，etc/wwwroot/maya/manual.html を参照の事。

以下，本家 readme.htmlより抜粋
> # Tvmaid MAYA
> Tvmaid MAYAは、Windows用録画予約プログラムです。  
> TVTestを使用して、番組表の取得、録画を行います。
>
> このプログラムは実験用で、暗号化されたデータを扱う機能はありません。  
> 
> ## ライセンス
> MITライセンスで公開します。  
> This software is released under the MIT License, see LICENSE.txt.

## 改造部分のライセンス
オリジナルのライセンスに倣い，MITライセンスとします。  
This software is released under the MIT License, see LICENSE.txt.

## 他作者の著作物のライセンス
オリジナルの"TvmaidMAYA"で使用している物の他に，以下の著作物が組み込まれています。

### CoreTweet
This software is licensed under the MIT License.  
[https://github.com/CoreTweet/CoreTweet](https://github.com/CoreTweet/CoreTweet)

### Newtonsoft.Json
Json.NET is open source under the MIT license and is free for commercial use.  
[https://raw.githubusercontent.com/JamesNK/Newtonsoft.Json/master/LICENSE.md](https://raw.githubusercontent.com/JamesNK/Newtonsoft.Json/master/LICENSE.md)

### MediaInfo.Native
MediaArea.net SARL  
BSD 2-Clause License  
[https://github.com/MediaArea/MediaInfoLib/blob/master/LICENSE](https://github.com/MediaArea/MediaInfoLib/blob/master/LICENSE)

### MediaInfo.DotNetWrapper
MIT License  
[https://raw.githubusercontent.com/StefH/MediaInfo.DotNetWrapper/master/LICENSE](https://raw.githubusercontent.com/StefH/MediaInfo.DotNetWrapper/master/LICENSE)

### NoImage.png
CoolTextで生成しました  
[https://ja.cooltext.com/](https://ja.cooltext.com/)

### maiddata/image/saya_*.png
CHARAT CHOCO で作成しました。  
[https://charat.me/chibi/](https://charat.me/chibi/)

### Google.Apis
Apache License Version 2.0  
[https://www.nuget.org/packages/Google.Apis/1.40.0/license](https://www.nuget.org/packages/Google.Apis/1.40.0/license)

### Google.Apis.Auth
Apache License Version 2.0  
[https://www.nuget.org/packages/Google.Apis.Auth/1.40.0/license](https://www.nuget.org/packages/Google.Apis.Auth/1.40.0/license)

### Google.Apis.Calendar.v3
Apache License Version 2.0  
[https://www.nuget.org/packages/Google.Apis.Calendar.v3/1.40.0.1590/license](https://www.nuget.org/packages/Google.Apis.Calendar.v3/1.40.0.1590/license)

### Google.Apis.Core
Apache License Version 2.0  
[https://www.nuget.org/packages/Google.Apis.Core/1.40.0/license](https://www.nuget.org/packages/Google.Apis.Core/1.40.0/license)

### System.Net.Http
MICROSOFT SOFTWARE LICENSE TERMS  
[https://dotnet.microsoft.com/dotnet_library_license.htm](https://dotnet.microsoft.com/dotnet_library_license.htm)

### System.Security.Cryptography.Algorithms
MICROSOFT SOFTWARE LICENSE TERMS  
[https://dotnet.microsoft.com/dotnet_library_license.htm](https://dotnet.microsoft.com/dotnet_library_license.htm)

### System.Security.Cryptography.Encoding
MICROSOFT SOFTWARE LICENSE TERMS  
[https://dotnet.microsoft.com/dotnet_library_license.htm](https://dotnet.microsoft.com/dotnet_library_license.htm)

### System.Security.Cryptography.Primitives
MICROSOFT SOFTWARE LICENSE TERMS  
[https://dotnet.microsoft.com/dotnet_library_license.htm](https://dotnet.microsoft.com/dotnet_library_license.htm)

### System.Security.Cryptography.X509Certificates
MICROSOFT SOFTWARE LICENSE TERMS  
[https://dotnet.microsoft.com/dotnet_library_license.htm](https://dotnet.microsoft.com/dotnet_library_license.htm)











