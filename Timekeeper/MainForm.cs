﻿using Codeplex.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Tvmaid
{
    public partial class MainForm : Form
    {
        string host;
        string macaddr = "";
        static int RecCount = -1; // 録画中の本数
        static int RevCount = -1; // 予約中の本数
        string appname = "";          // keii AppNmae取り置き用
        static bool ViewMode = false; // keii 全部表示/有効のみ
        static int AlarmTime = 0;     // keii 録画開始ｘ分前にアラーム出す(0で出さない)

        List<Reserve> reserves = new List<Reserve>();
        PairList define;
        Timer timer;

        class Reserve
        {
            public string Title = "";
            public string Service = "";
            public string Time = "";
            public int Status = 0;
            public string Tuner = "";
            public int Id = 0;     // keii
            public DateTime Start; // keii
        }

        enum ReserveStatus
        {
            Enable = 1,     // 有効
            EventMode = 2,  // 番組追従
            Duplication = 32,   // チューナ重複
            Recoding = 64,  // 録画中
            Complete = 128, // 録画完了
            Failed = 256    // keii (Tvmaid未起動などで)録画失敗
        };

        public MainForm()
        {
            InitializeComponent();

            EnableDoubleBuffer(listView);

            try
            {
                appname = this.Text;
                LoadDefine();
                this.Text = host + " - " + this.Text;
            }
            catch (Exception e)
            {
                MessageBox.Show("起動に失敗しました。[詳細] " + e.Message);
                return;
            }

            timer = new Timer();

            timer.Tick += Reload;
            timer.Interval = 2000; // 2sec
            timer.Enabled = true;
        }

        //ダブルバッファ
        static void EnableDoubleBuffer(Control c)
        {
            var prop = c.GetType().GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            prop.SetValue(c, true, null);
        }
        
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                define["host"] = host;
                if(macaddr!="")
                    define["macaddr"] = macaddr;

                define["font"] = Font.FontFamily.Name;
                define["fontsize"] = Font.SizeInPoints.ToString();

                if(this.WindowState == FormWindowState.Normal)
                {
                    define["left"] = this.Left.ToString();
                    define["top"] = this.Top.ToString();
                    define["width"] = this.Width.ToString();
                    define["height"] = this.Height.ToString();
                }
                else
                {
                    define["left"] = this.RestoreBounds.Left.ToString();
                    define["top"] = this.RestoreBounds.Top.ToString();
                    define["width"] = this.RestoreBounds.Width.ToString();
                    define["height"] = this.RestoreBounds.Height.ToString();
                }

                define["title.width"] = this.titleHeader.Width.ToString();
                define["service.width"] = this.serviceHeader.Width.ToString();
                define["time.width"] = this.timeHeader.Width.ToString();
                define["status.width"] = this.statusHeader.Width.ToString();
                define["tuner.width"] = this.tunerHeader.Width.ToString();

                define["viewmode"] = ViewMode  ? "true" : "false";
                define["alarmtime"] = AlarmTime.ToString();

                define.Save();
            }
            catch { }
        }

        void LoadDefine()
        {
            var path = Util.GetBasePath("Timekeeper.def");

            define = new PairList(path);

            if (File.Exists(path) == false)
            {
                host = "localhost:20001";
                return;
            }

            define.Load();

            host = define["host"];

            var fam = new System.Drawing.FontFamily(define["font"]);
            base.Font = new System.Drawing.Font(fam, define.GetFloat("fontsize"));

            Left = define.GetInt("left");
            Top = define.GetInt("top");
            Width = define.GetInt("width");
            Height = define.GetInt("height");
            
            var d = Screen.GetWorkingArea(this);
            int h = d.Height;
            int w =d.Width;
            if (Left < 0 && Left + Width < 0 && Top < 0 && Top + Height < 0)
            {
                Left = 0;
                Top = 0;
                Width = 747;
                Height = 212;
            }
            else if (h < Top || w < Left)
            {
                Left = 0;
                Top = 0;
            }

            titleHeader.Width = define.GetInt("title.width");
            serviceHeader.Width = define.GetInt("service.width");
            timeHeader.Width = define.GetInt("time.width");
            statusHeader.Width = define.GetInt("status.width");
            tunerHeader.Width = define.GetInt("tuner.width");

            if (define.IsDefined("viewmode"))
                ViewMode = define["viewmode"] == "true" ? true : false;

            if(define.IsDefined("alarmtime"))
                AlarmTime = define.GetInt("alarmtime");

            if(!System.Text.RegularExpressions.Regex.IsMatch(host,@"localhost") && !System.Text.RegularExpressions.Regex.IsMatch(host, @"127\.0\.0\.1"))
                if (define.IsDefined("macaddr"))
                {
                    macaddr = define["macaddr"];
                    toolStripSeparator2.Visible = true;
                    wOLToolStripMenuItem.Visible = true;
                    goSleepToolStripMenuItem.Visible = true;
                }

            ViewModeText(ViewMode);
        }

        private void listView_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            lock (reserves)
            {
                try
                {
                    Reserve res;

                    //リストにないときでも、e.Itemをセットせずにコールバックから出てはいけない
                    if (e.ItemIndex >= reserves.Count)
                        res = new Reserve();
                    else
                        res = reserves[e.ItemIndex];

                    var item = new ListViewItem();

                    item.Text = res.Title;
                    item.SubItems.Add(res.Service);
                    item.SubItems.Add(res.Time);

                    var status = "正常";

                    if ((res.Status & (int)ReserveStatus.Duplication) > 0)
                    {
                        status = "重複";
                        item.BackColor = System.Drawing.Color.Gold;
                    }

                    if ((res.Status & (int)ReserveStatus.Recoding) > 0)
                    {
                        status = "録画中";
                        item.BackColor = System.Drawing.Color.LightCoral;
                    }

                    if ((res.Status & (int)ReserveStatus.Enable) == 0)
                    {
                        status = "無効";
                        item.BackColor = System.Drawing.Color.LightGray;
                    }

                    item.SubItems.Add(status);
                    item.SubItems.Add(res.Tuner);
                    item.SubItems.Add(res.Id.ToString());

                    e.Item = item;
                }
                catch { }
            }
        }
        
        async void Reload(object sender, EventArgs e)
        {
            try
            {
                var client = new WebClient();

                client.Encoding = Encoding.UTF8;

                var sql = "select title, service.name, start, end, status, tuner, reserve.id from reserve"
                        + " left join service on reserve.fsid = service.fsid"
                        + " where {1} end > {0} group by reserve.id order by start".Formatex(DateTime.Now.Ticks, (ViewMode ? "": "status & 1 and"));

                sql = System.Web.HttpUtility.UrlEncode(sql, Encoding.UTF8);

                var url = "http://" + host + "/webapi/GetTable?sql=" + sql;
                var data = await client.DownloadStringTaskAsync(url);
                var ret = DynamicJson.Parse(data);
                int cnt = 0;
                var total = new TimeSpan(); //rel27 総予約時間
                var count = 0;              //rel27 予約本数

                lock (reserves)
                {
                    if (ret.code != 0)
                        throw new Exception(ret.message);

                    reserves.Clear();

                    var list = (dynamic[])ret.data1;
                    count = list.Length;

                    foreach (var item in list)
                    {
                        var res = new Reserve();
 
                        res.Title = item[0];
                        res.Service = item[1];

                        var start = new DateTime((long)item[2]);
                        var end = new DateTime((long)item[3]);
                        res.Time = start.ToString("dd(ddd) HH:mm-") + end.ToString("HH:mm");
                        total += end - start;

                        res.Status = (int)item[4];
                        res.Tuner = item[5];

                        res.Id = (int)item[6]; // keii
                        res.Start = start;

                        if ((StatusConv(res.Status) & ReserveStatus.Recoding) != 0) ++cnt;

                        reserves.Add(res);
                    }
                }

                listView.VirtualListSize = reserves.Count;
                listView.Invalidate();

                /*rel27
                url = "http://" + host + "/webapi/GetRecordFolderFree";
                data = await client.DownloadStringTaskAsync(url);
                ret = DynamicJson.Parse(data);

                if (ret.code == 0)
                {
                    var freeDisk = ret.data1 / 1024 / 1024 / 1024;
                    var freeTime = ret.data1 / 2048 / 1024 / 60;
                    statusText.Text = string.Format("ok, 予約 {2} ({3:0} 分), 空き {0:0.00} GB ({1:0} 分)", freeDisk, freeTime, count, total.TotalMinutes);
                }
                else
                    statusText.Text = "ok";
                */

                statusText.Text = "ok";

                // HDD容量取得(録画開始時と予約数が変化した時，取得する)
                if (RecCount != cnt && RevCount != reserves.Count)
                {
                    url = "http://" + host + "/webapi/GetRecordFolderFree";
                    data = await client.DownloadStringTaskAsync(url);
                    ret = DynamicJson.Parse(data);
                    this.Text = host + " - " + appname + " [ " + (ret.data1).ToString("0,,,.00") + " GB ]";
                    RecCount = cnt;
                    RevCount = reserves.Count;

                    // "次の予約" を書き換え
                    foreach (var reserve in reserves)
                    {
                        if ((StatusConv(reserve.Status) & ReserveStatus.Enable) > 0)
                        {
                            var x = ("次の予約…" + " " + reserve.Time + "\r\n" + reserve.Title);
                            if (x.Length > 64)
                                x = x.Substring(0, 62) + "…";

                            notifyIcon.Text = x;
                            break;
                        }
                    }
                }

                // 予約ｘ分前で…
                if (AlarmTime > 0)
                {
                    DateTime now = DateTime.Now.AddMinutes(AlarmTime);
                    foreach (var reserve in reserves)
                    {
                        //AlarmTime
                        if (reserve.Start < now.AddSeconds(2) && reserve.Start > now)
                        {
                            if ((StatusConv(reserve.Status) & ReserveStatus.Enable) > 0)
                            {
                                DisplayNotifyIcon("もうすぐ録画が始まります…", reserve.Start.ToString("HH:mm-").ToZenAlphaNum() + "\r\n" + reserve.Title);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                listView.VirtualListSize = 0;
                listView.Invalidate();

                statusText.Text = ex.Message;
            }
        }
        
        private void DisplayNotifyIcon(string title, string text, ToolTipIcon icon = ToolTipIcon.Info)
        {
            notifyIcon.Visible = true;
            notifyIcon.BalloonTipIcon = icon;
            notifyIcon.BalloonTipTitle = title;
            notifyIcon.BalloonTipText = text;
            notifyIcon.ShowBalloonTip(3000);
            notifyIcon.Visible = false;
        }

        private void ViewModeText(bool mode)
        {
            ChangeViewModeMenuItem.Text = mode ? "有効のみ表示" : "全て表示";
        }
        
        private void fontChangeMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new FontDialog();

            dialog.Font = listView.Font;

            if (dialog.ShowDialog() != DialogResult.Cancel)
                Font = dialog.Font;
        }

        // 『予約』を開く
        private void OpenReserveMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://" + host + "/maya/index.html#reserve");
        }

        private void notifyIcon_Click(object sender, EventArgs e)
        {
            this.Visible = true;

            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.Activate();
                notifyIcon.Visible = false;
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Visible = false;
                notifyIcon.Visible = true;
            }
        }
        
        // 有効/無効の切り替え
        private void ChangeReserveStateMenuItem_Click(object sender, EventArgs e)
        {
            lock (reserves)
            {
                if (listView.SelectedIndices.Count > 0)
                {
                    int index = listView.SelectedIndices[0];
                    var id = reserves[index].Id;

                    if ((StatusConv(reserves[index].Status) & ReserveStatus.Recoding) != 0)
                    {
                        DialogResult result = MessageBox.Show("録画中の予約です。\r\n\"無効\"にすると録画が中止されます。\r\n\r\nよろしいですか？",
                        "録画をキャンセルしようとしています",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2);

                        if (result != DialogResult.OK)
                            return;
                    }

                    var client = new WebClient();
                    client.Encoding = Encoding.UTF8;
                    
                    var url = "http://" + host + "/webapi/ChangeReserveState?id=" + id.ToString();
                    var data = client.DownloadString(url);

                    url = "http://" + host + "/webapi/ResetReserveTuner";
                    data = client.DownloadString(url);
                    client.Dispose();

                    Reload(sender, e);
                }
            }
        }

        // 有効のみ/全部表示切替
        private void ChangeViewModeMenuItem_Click(object sender, EventArgs e)
        {
            ViewMode = !ViewMode;
            ViewModeText(ViewMode);
            Reload(sender, e);
        }

        private ReserveStatus StatusConv(int status)
        {
            return (ReserveStatus)Enum.ToObject(typeof(ReserveStatus), status);
        }

        // WoL
        private void wOLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (macaddr != "")
            {
                var mac = Wol.GetMacArray(macaddr);
                Wol.Send(mac);
            }
        }

        // Sleepモードへ移行
        private void goSleepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // return { "code":0,"message":"","data1":null}
            DialogResult result = MessageBox.Show("Tvmaidをスリープモードへ移行しますか？",
                "Go Sleep !",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2);

            if (result != DialogResult.OK)
                return;

            var client = new WebClient();
            client.Encoding = Encoding.UTF8;

            var url = "http://" + host + "/webapi/GoSleep";
            var data = client.DownloadString(url);
            var ret = DynamicJson.Parse(data);

            if (ret.code != 0)
                throw new Exception(ret.message);

            client.Dispose();
        }
    }
    

}
