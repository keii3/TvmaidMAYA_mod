﻿namespace Tvmaid
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.listView = new System.Windows.Forms.ListView();
            this.titleHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.serviceHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.statusHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tunerHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openReserveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeReserveStateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ChangeViewModeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontChangeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.wOLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goSleepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusbar = new System.Windows.Forms.StatusStrip();
            this.statusText = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextMenu.SuspendLayout();
            this.statusbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Timekeeper";
            this.notifyIcon.Click += new System.EventHandler(this.notifyIcon_Click);
            // 
            // listView
            // 
            this.listView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.titleHeader,
            this.serviceHeader,
            this.timeHeader,
            this.statusHeader,
            this.tunerHeader});
            this.listView.ContextMenuStrip = this.contextMenu;
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.FullRowSelect = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.ShowItemToolTips = true;
            this.listView.Size = new System.Drawing.Size(731, 151);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.VirtualMode = true;
            this.listView.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.listView_RetrieveVirtualItem);
            // 
            // titleHeader
            // 
            this.titleHeader.Text = "タイトル";
            this.titleHeader.Width = 250;
            // 
            // serviceHeader
            // 
            this.serviceHeader.Text = "サービス";
            this.serviceHeader.Width = 97;
            // 
            // timeHeader
            // 
            this.timeHeader.Text = "予約日時";
            this.timeHeader.Width = 150;
            // 
            // statusHeader
            // 
            this.statusHeader.Text = "状態";
            this.statusHeader.Width = 115;
            // 
            // tunerHeader
            // 
            this.tunerHeader.Text = "チューナ";
            this.tunerHeader.Width = 113;
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openReserveMenuItem,
            this.ChangeReserveStateMenuItem,
            this.toolStripSeparator1,
            this.ChangeViewModeMenuItem,
            this.fontChangeMenuItem,
            this.toolStripSeparator2,
            this.wOLToolStripMenuItem,
            this.goSleepToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(217, 170);
            // 
            // openReserveMenuItem
            // 
            this.openReserveMenuItem.Name = "openReserveMenuItem";
            this.openReserveMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openReserveMenuItem.Size = new System.Drawing.Size(216, 22);
            this.openReserveMenuItem.Text = "予約を開く(&O)";
            this.openReserveMenuItem.Click += new System.EventHandler(this.OpenReserveMenuItem_Click);
            // 
            // ChangeReserveStateMenuItem
            // 
            this.ChangeReserveStateMenuItem.Name = "ChangeReserveStateMenuItem";
            this.ChangeReserveStateMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.ChangeReserveStateMenuItem.Size = new System.Drawing.Size(216, 22);
            this.ChangeReserveStateMenuItem.Text = "有効/無効の切替(&R)";
            this.ChangeReserveStateMenuItem.Click += new System.EventHandler(this.ChangeReserveStateMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(213, 6);
            // 
            // ChangeViewModeMenuItem
            // 
            this.ChangeViewModeMenuItem.Name = "ChangeViewModeMenuItem";
            this.ChangeViewModeMenuItem.Size = new System.Drawing.Size(216, 22);
            this.ChangeViewModeMenuItem.Text = "表示切替";
            this.ChangeViewModeMenuItem.Click += new System.EventHandler(this.ChangeViewModeMenuItem_Click);
            // 
            // fontChangeMenuItem
            // 
            this.fontChangeMenuItem.Name = "fontChangeMenuItem";
            this.fontChangeMenuItem.Size = new System.Drawing.Size(216, 22);
            this.fontChangeMenuItem.Text = "フォント変更(&F)...";
            this.fontChangeMenuItem.Click += new System.EventHandler(this.fontChangeMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(213, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // wOLToolStripMenuItem
            // 
            this.wOLToolStripMenuItem.Name = "wOLToolStripMenuItem";
            this.wOLToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.wOLToolStripMenuItem.Text = "WOL";
            this.wOLToolStripMenuItem.Visible = false;
            this.wOLToolStripMenuItem.Click += new System.EventHandler(this.wOLToolStripMenuItem_Click);
            // 
            // goSleepToolStripMenuItem
            // 
            this.goSleepToolStripMenuItem.Name = "goSleepToolStripMenuItem";
            this.goSleepToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.goSleepToolStripMenuItem.Text = "Go Sleep";
            this.goSleepToolStripMenuItem.Visible = false;
            this.goSleepToolStripMenuItem.Click += new System.EventHandler(this.goSleepToolStripMenuItem_Click);
            // 
            // statusbar
            // 
            this.statusbar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusText});
            this.statusbar.Location = new System.Drawing.Point(0, 151);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(731, 22);
            this.statusbar.TabIndex = 11;
            // 
            // statusText
            // 
            this.statusText.Name = "statusText";
            this.statusText.Size = new System.Drawing.Size(36, 17);
            this.statusText.Text = "ready";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 173);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.statusbar);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "タイムキーパー";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.contextMenu.ResumeLayout(false);
            this.statusbar.ResumeLayout(false);
            this.statusbar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader titleHeader;
        private System.Windows.Forms.ColumnHeader timeHeader;
        private System.Windows.Forms.ColumnHeader statusHeader;
        private System.Windows.Forms.ColumnHeader tunerHeader;
        private System.Windows.Forms.ColumnHeader serviceHeader;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem fontChangeMenuItem;
        private System.Windows.Forms.StatusStrip statusbar;
        private System.Windows.Forms.ToolStripStatusLabel statusText;
        private System.Windows.Forms.ToolStripMenuItem openReserveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChangeReserveStateMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ChangeViewModeMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem wOLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goSleepToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon;
    }
}

